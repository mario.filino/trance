package trance.objective;

import trance.progs.Backward;
import trance.progs.OracleParser;
import trance.progs.Parser;
import trance.progs.State;

public class MarginMax extends Margin{
	public double margin(Parser candidates, OracleParser oracles, boolean marginAll){
		if(candidates.agenda().size() != oracles.agenda().size())
			throw new RuntimeException("invalid candidate and oracle pair");
		
		int agendaSize = candidates.agenda().size();
		
		if(marginAll){			
			int candidateKbestSize = candidates.agenda().get(agendaSize - 1).size();
			int oracleKbestSize = oracles.agenda().get(agendaSize - 1).size();
			
			double zCandidates = 0.0;
			double zOracle = 0.0;
			
			double maxScore = Double.NEGATIVE_INFINITY;
			
			for(int i = 0; i < oracleKbestSize; i++){
				maxScore = Math.max(maxScore, oracles.agenda().get(agendaSize - 1).get(i).currentScore());
				zOracle += Math.exp(oracles.agenda().get(agendaSize - 1).get(i).currentScore());
			}
			
			for(int i = 0; i < candidateKbestSize; i++){
				if(candidates.agenda().get(agendaSize - 1).get(i).currentScore() > maxScore){
					zCandidates += Math.exp(candidates.agenda().get(agendaSize - 1).get(i).currentScore());
				}
			}
			
			boolean found = false;
			double loss = 0.0;
			
			for(int c = 0; c < candidateKbestSize; c++){
				if(candidates.agenda().get(agendaSize - 1).get(c).currentScore() > maxScore){
					for(int o = 0; o < oracleKbestSize; o++){
						State candidateState = candidates.agenda().get(agendaSize - 1).get(c);
						State oracleState = oracles.agenda().get(agendaSize - 1).get(o);
						
						double maxError = 0.0;
						State maxCandidateState = null;
						State maxOracleState = null;
						
						while(candidateState != null && oracleState != null){
							if(candidateState.step() > oracleState.step()){
								candidateState = candidateState.derivation();
							}else if(oracleState.step() > candidateState.step()){
								oracleState = oracleState.derivation();
							}else{
								double candidateScore = candidateState.currentScore();
								double oracleScore = oracleState.currentScore();
								
								boolean suffered = candidateScore > oracleScore;
								double error = Math.max(0.0, 1.0 - (oracleScore - candidateScore));
								
								if(suffered && error > maxError){
									maxError = error;
									maxCandidateState = candidateState;
									maxOracleState = oracleState;
								}
								
								candidateState = candidateState.derivation();
								oracleState = oracleState.derivation();
							}
						}
						
						if(maxCandidateState != null && maxOracleState != null){
							double candidateScore = candidates.agenda().get(agendaSize - 1).get(c).currentScore();
							double oracleScore = oracles.agenda().get(agendaSize - 1).get(o).currentScore();
							
							double candidateProb = Math.exp(candidateScore) / zCandidates;
							double oracleProb = Math.exp(oracleScore) / zOracle;
							
							double lossFactor = candidateProb * oracleProb;
							
							backward.put(maxCandidateState, new Backward());
							backward.put(maxOracleState, new Backward());
							
							backward.get(maxCandidateState).loss += lossFactor;
							backward.get(maxOracleState).loss -= lossFactor;
							
							loss += maxError * lossFactor;
							found = true;
						}
					}
				}
			}
			
			if(!found) return 0.0;
			
			for(int c = 0; c < candidateKbestSize; c++){
				if(candidates.agenda().get(agendaSize - 1).get(c).currentScore() > maxScore){
					states.get(candidates.agenda().get(agendaSize - 1).get(c).step()).add(candidates.agenda().get(agendaSize - 1).get(c));
				}
			}
			
			for(int o = 0; o < oracleKbestSize; o++){
				states.get(oracles.agenda().get(agendaSize - 1).get(o).step()).add(oracles.agenda().get(agendaSize - 1).get(o));
			}
			
			return loss;
		}else{
			int lastCandidateSize = candidates.agenda().get(agendaSize - 1).size();
			int lastOracleSize = oracles.agenda().get(agendaSize - 1).size();
			
			State candidateState = candidates.agenda().get(agendaSize - 1).get(lastCandidateSize - 1);
			State oracleState = oracles.agenda().get(agendaSize - 1).get(lastOracleSize - 1);
			
			double maxError = 0.0;
			State maxCandidateState = null;
			State maxOracleState = null;
			
			while(candidateState != null && oracleState != null){
				if(candidateState.step() > oracleState.step()){
					candidateState = candidateState.derivation();
				}else if(oracleState.step() > candidateState.step()){
					oracleState = oracleState.derivation();
				}else{
					double candidateScore = candidateState.currentScore();
					double oracleScore = oracleState.currentScore();
					
					boolean suffered = candidateScore > oracleScore;
					double error = Math.max(0.0, 1.0 - (oracleScore - candidateScore));
					
					if(suffered && error > maxError){
						maxError = error;
						maxCandidateState = candidateState;
						maxOracleState = oracleState;
					}
					
					candidateState = candidateState.derivation();
					oracleState = oracleState.derivation();
				}
			}
			
			if(maxCandidateState == null || maxOracleState == null) return 0.0;
			
			backward.put(maxCandidateState, new Backward());
			backward.put(maxOracleState, new Backward());
			
			backward.get(maxCandidateState).loss += 1.0;
			backward.get(maxOracleState).loss -= 1.0;
			
			states.get(maxCandidateState.step()).add(maxCandidateState);
			states.get(maxOracleState.step()).add(maxOracleState);
			
			return maxError;
		}
	}
}
