package trance.objective;

import java.util.Map;

import org.jblas.FloatMatrix;

import trance.model.Model;
import trance.progs.Backward;
import trance.progs.Objective;
import trance.progs.Operation;
import trance.progs.OracleParser;
import trance.progs.Parser;
import trance.progs.State;

public abstract class Margin extends Objective{
	public Margin(){}
	
	public double execute(trance.model.Model param, Parser candidates, OracleParser oracles, trance.gradient.Model g) throws InterruptedException{
		initialize(candidates, oracles);
		
		double loss = margin(candidates, oracles, true);
		
		if(!backward.isEmpty()){
			propagate(param, candidates, oracles, g);
		}
		
		return loss;
	}
	
	public double execute(trance.gradient.Model g){
		return 0.0;
	}
	
	public abstract double margin(Parser candidates, OracleParser oracles, boolean MarginAll);
	
	private void resize(int rows, int cols){
		queue = new FloatMatrix[cols];
		
		for(int i = 0; i < cols; i++){
			queue[i] = new FloatMatrix(rows, 1);
		}
	}
	
	public void propagate(trance.model.Model param, Parser candidates, OracleParser oracles, trance.gradient.Model g) throws InterruptedException{
		++g.count;
		
		resize(candidates.hidden(), candidates.inputSize() + 1);
		
		for(int step = states.size() - 1; step >= 0; step--){
			//System.out.println("step: " + step);
			
			for(int i = 0; i < states.get(step).size(); i++){
				State state = states.get(step).get(i);
				Backward backward = backwardSate(param, state);
								
				if(state.operation() != Operation.AXIOM){
					if(state.operation() == Operation.SHIFT){
						// classification
						FloatMatrix Wc = g.getVsh(state.label());
						
						Wc.addi(state.layer().transpose().mul((float) backward.loss));
						g.getvsh().put(state.label(), 0, g.getvsh(state.label()) + (float) backward.loss);
						
						// propagate to delta
						backward.delta.addi(Model.deactivation(state.layer()).mul(param.getVsh(state.label()).transpose().mul((float) backward.loss)));
					}else if(state.operation() == Operation.REDUCE){
						// classification
						FloatMatrix Wc = g.getVre(state.label());
						
						Wc.addi(state.layer().transpose().mul((float) backward.loss));
						g.getvre().put(state.label(), 0, g.getvre(state.label()) + (float) backward.loss);
						
						// propagate to delta
						backward.delta.addi(Model.deactivation(state.layer()).mul(param.getVre(state.label()).transpose().mul((float) backward.loss)));
					}else if(state.operation() == Operation.UNARY || state.operation() == Operation.FINAL || state.operation() == Operation.IDLE){
						// classification
						FloatMatrix Wc = g.getVun(state.label());
						
						Wc.addi(state.layer().transpose().mul((float) backward.loss));
						g.getvun().put(state.label(), 0, g.getvun(state.label()) + (float) backward.loss);
						
						// propagate to delta
						backward.delta.addi(Model.deactivation(state.layer()).mul(param.getVun(state.label()).transpose().mul((float) backward.loss)));
					}
				}
				
				switch(state.operation()){
					case Operation.AXIOM: {
						g.getBa().addi(backward.delta);
					} break;
					
					case Operation.SHIFT: {
						FloatMatrix Hsh = g.getHsh(state.label());
						FloatMatrix Qsh = g.getQsh(state.label());
						FloatMatrix Wsh = g.getWsh(state.label());
						FloatMatrix Bsh = g.getBsh(state.label());
						
						Hsh.addi(backward.delta.mmul(state.derivation().layer().transpose()));
						Qsh.addi(backward.delta.mmul(candidates.queue().col(state.queueIndex()).transpose()));
						Wsh.addi(backward.delta.mmul(param.terminal(state.head()).transpose()));
						Bsh.addi(backward.delta);
						
						// propagate to ancedent
						backwardState(param, state.derivation(), backward.loss).delta.
							addi(Model.deactivation(state.derivation().layer()).
								mul(param.getHsh(state.label()).transpose().mmul(backward.delta)));
						
						queue[state.queueIndex()].
							addi(Model.deactivation(candidates.queue().col(state.queueIndex())).
								mul(param.getQsh(state.label()).transpose().mmul(backward.delta)));
						
						//System.out.println("shift queue index: " + state.queueIndex());
						
						int index = states.get(state.derivation().step()).indexOf(state.derivation());
						
						// register state
						if(index == -1){
							states.get(state.derivation().step()).add(state.derivation());
						}else{
							states.get(state.derivation().step()).set(index, state.derivation());
						}
					} break;
					
					case Operation.REDUCE: {
						FloatMatrix Hre = g.getHre(state.label());
						FloatMatrix Qre = g.getQre(state.label());
						FloatMatrix Wre0 = g.getWre0(state.label());
						FloatMatrix Wre1 = g.getWre1(state.label());
						FloatMatrix Bre = g.getBre(state.label());
						
						Hre.addi(backward.delta.mmul(state.stack().layer().transpose()));
						Qre.addi(backward.delta.mmul(candidates.queue().col(state.queueIndex()).transpose()));
						Wre0.addi(backward.delta.mmul(state.derivation().layer().transpose()));
						Wre1.addi(backward.delta.mmul(state.reduced().layer().transpose()));
						Bre.addi(backward.delta);
						
						// propagate to ancedent
						backwardSate(param, state.stack()).delta.
							addi(Model.deactivation(state.stack().layer()).
								mul(param.getHre(state.label()).transpose().mmul(backward.delta)));
						
						queue[state.queueIndex()].
							addi(Model.deactivation(candidates.queue().col(state.queueIndex())).
								mul(param.getQre(state.label()).transpose().mmul(backward.delta)));
						
						//System.out.println("reduce queue index: " + state.queueIndex());
						
						backwardState(param, state.derivation(), backward.loss).delta.
							addi(Model.deactivation(state.derivation().layer()).
								mul(param.getWre0(state.label()).transpose().mmul(backward.delta)));
						
						backwardSate(param, state.reduced()).delta.
							addi(Model.deactivation(state.reduced().layer()). 
								mul(param.getWre1(state.label()).transpose().mmul(backward.delta)));

						int index = states.get(state.derivation().step()).indexOf(state.derivation());
						
						// register state
						if(index == -1){
							states.get(state.derivation().step()).add(state.derivation());
						}else{
							states.get(state.derivation().step()).set(index, state.derivation());
						}									
					} break;
					
					case Operation.UNARY: {
						FloatMatrix Hun = g.getHun(state.label());
						FloatMatrix Qun = g.getQun(state.label());
						FloatMatrix Wun = g.getWun(state.label());
						FloatMatrix Bun = g.getBun(state.label());
						
						Hun.addi(backward.delta.mmul(state.stack().layer().transpose()));
						Qun.addi(backward.delta.mmul(candidates.queue().col(state.queueIndex()).transpose()));
						Wun.addi(backward.delta.mmul(state.derivation().layer().transpose()));
						Bun.addi(backward.delta);

						// propagate to ancedent
						backwardSate(param, state.stack()).delta.
							addi(Model.deactivation(state.stack().layer()).
								mul(param.getHun(state.label()).transpose().mmul(backward.delta)));
						
						queue[state.queueIndex()].
							addi(Model.deactivation(candidates.queue().col(state.queueIndex())).
								mul(param.getQun(state.label()).transpose().mmul(backward.delta)));

						//System.out.println("unary queue index: " + state.queueIndex());
						
						backwardState(param, state.derivation(), backward.loss).delta.
							addi(Model.deactivation(state.derivation().layer()).
								mul(param.getWun(state.label()).transpose().mmul(backward.delta)));			
						
						int index = states.get(state.derivation().step()).indexOf(state.derivation());
						
						// register state
						if(index == -1){
							states.get(state.derivation().step()).add(state.derivation());
						}else{
							states.get(state.derivation().step()).set(index, state.derivation());
						}	
					} break;
					
					case Operation.FINAL: {
						g.getWf().addi(backward.delta.mmul(state.derivation().layer().transpose()));
						g.getBf().addi(backward.delta);
						
						// propagate to ancedent
						backwardState(param, state.derivation(), backward.loss).delta.
							addi(Model.deactivation(state.derivation().layer()).
								mul(param.getWf().transpose().mmul(backward.delta)));
						
						int index = states.get(state.derivation().step()).indexOf(state.derivation());
						
						// register state
						if(index == -1){
							states.get(state.derivation().step()).add(state.derivation());
						}else{
							states.get(state.derivation().step()).set(index, state.derivation());
						}
					} break;
					
					case Operation.IDLE: {
						g.getWi().addi(backward.delta.mmul(state.derivation().layer().transpose()));
						g.getBi().addi(backward.delta);
						
						// propagate to ancedent
						backwardState(param, state.derivation(), backward.loss).delta.
							addi(Model.deactivation(state.derivation().layer()).
								mul(param.getWi().transpose().mmul(backward.delta)));
						
						int index = states.get(state.derivation().step()).indexOf(state.derivation());
						
						// register state
						if(index == -1){
							states.get(state.derivation().step()).add(state.derivation());
						}else{
							states.get(state.derivation().step()).set(index, state.derivation());
						}	
					} break;
					
					default:
						throw new RuntimeException("action not valid");
				}
			}
		}
		
		String[] input = candidates.sentence();
		
		// propagate queue contexts
		for(int index = 0; index < input.length; index++){	
			g.getHqu().addi(queue[index].mmul(candidates.queue().col(index + 1).transpose()));
			g.getWqu().addi(queue[index].mmul(param.terminal(input[index]).transpose()));
			g.getBqu().addi(queue[index]);
			
			queue[index + 1].addi(Model.deactivation(candidates.queue().col(index + 1)).
				mul(param.getHqu().transpose().mmul(queue[index])));
		}
		
		g.getBqe().addi(queue[input.length]);
	}
}
