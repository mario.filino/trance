package trance.objective;

import trance.progs.Backward;
import trance.progs.OracleParser;
import trance.progs.Parser;
import trance.progs.State;
import trance.semiring.Logprob;

public abstract class Violation extends Margin{
	public Violation(){}
	
	public abstract int violation(Parser candidates, OracleParser oracles);	
	
	public double margin(Parser candidates, OracleParser oracles, boolean marginAll){
		if(candidates.agenda().size() != oracles.agenda().size())
			throw new RuntimeException("invalid candidate and oracle pair");
		
		int step = violation(candidates, oracles);
		 
		if(step == -1) return 0.0;
		
		if(marginAll){
			Logprob zCandidate = new Logprob();
			Logprob zOracle = new Logprob();
			
			double maxScore = Double.NEGATIVE_INFINITY;
			
			for(int o = 0; o < oracles.agenda().get(step).size(); o++){
				maxScore = Math.max(maxScore, oracles.agenda().get(step).get(o).currentScore());
				
				//System.out.println("z_oracle prev: " + zOracle);
				zOracle.plus(oracles.agenda().get(step).get(o).currentScore()); 
				//System.out.println("z_oracle after: " + zOracle);
			}
			
			for(int c = 0; c < candidates.agenda().get(step).size() ; c++){
				if(candidates.agenda().get(step).get(c).currentScore() > maxScore){
					
					//System.out.println("z_candidate prev: " + zCandidate);
					zCandidate.plus(candidates.agenda().get(step).get(c).currentScore());
					//System.out.println("z_candidate after: " + zCandidate);
				}
			}
			
			double loss = 0.0;
			
			for(int c = 0; c < candidates.agenda().get(step).size() ; c++){
				if(candidates.agenda().get(step).get(c).currentScore() > maxScore){
					for(int o = 0; o < oracles.agenda().get(step).size(); o++){
						State candidateState = candidates.agenda().get(step).get(c);
						State oracleState = oracles.agenda().get(step).get(o);
						
						double candidateScore = candidateState.currentScore();
						double oracleScore = oracleState.currentScore();
						
						boolean suffered = candidateScore > oracleScore;
						double error = Math.max(0.0, 1.0 - (oracleScore - candidateScore));
						
						if(!suffered || error <= 0.0) continue;
						
						/*System.out.println("candidate score: " + candidateScore);
						System.out.println("oracle score: " + oracleScore);
						
						System.out.println("z_candidate: " + zCandidate);
						System.out.println("z_oracle: " + zOracle);*/
						
						Logprob candidateProb = new Logprob(candidateScore).div(zCandidate.value());
						Logprob oracleProb = new Logprob(oracleScore).div(zOracle.value());
						
						/*System.out.println("prob_candidate: " + candidateProb);
						System.out.println("prob_oracle: " + oracleProb);*/
						
						double lossFactor = Math.exp(oracleProb.mul(candidateProb.value()).value());
						
						//System.out.println("loss factor: " + lossFactor);
						
						Backward candidateStateBack = new Backward();
						Backward oracleStateBack = new Backward();
						
						candidateStateBack.loss += lossFactor;
						oracleStateBack.loss -= lossFactor;
						
						backward.put(candidateState, candidateStateBack);
						backward.put(oracleState, oracleStateBack);
						
						loss += error * lossFactor;
						
						//System.out.println("error: " + error + " loss factor: " + lossFactor + " loss: " + loss);
					}
				}
			}
			
			for(int c = 0; c < candidates.agenda().get(step).size(); c++){
				if(candidates.agenda().get(step).get(c).currentScore() > maxScore){
					states.get(candidates.agenda().get(step).get(c).step()).add(candidates.agenda().get(step).get(c));
				}
			}
			
			for(int o = 0; o < oracles.agenda().get(step).size(); o++){
				states.get(oracles.agenda().get(step).get(o).step()).add(oracles.agenda().get(step).get(o));
			}
			
			return loss;
		}else{
			int stepCandidateSize = candidates.agenda().get(step).size();
			int stepOracleSize = oracles.agenda().get(step).size();
			
			State candidateState = candidates.agenda().get(step).get(stepCandidateSize - 1);
			State oracleState = oracles.agenda().get(step).get(stepOracleSize - 1);
			
			backward.put(candidateState, new Backward());
			backward.put(oracleState, new Backward());
			
			backward.get(candidateState).loss += 1.0;
			backward.get(oracleState).loss -= 1.0;
			
			states.get(candidateState.step()).add(candidateState);
			states.get(oracleState.step()).add(oracleState);
			
			return Math.max(0.0, 1.0 - (oracleState.currentScore() - candidateState.currentScore()));
		}
	}
}
