package trance.objective;

import trance.progs.OracleParser;
import trance.progs.Parser;

public class ViolationMax extends Violation{

	@Override
	public int violation(Parser candidates, OracleParser oracles){
		int stepMax = -1;
		double errorMax = 0.0;
		
		for(int step = 0; step < oracles.agenda().size(); step++){
			if(!candidates.agenda().get(step).isEmpty() && !oracles.agenda().get(step).isEmpty()){
				int lastCandidate = candidates.agenda().get(step).size() - 1;
				int lastOracle = oracles.agenda().get(step).size() - 1;
				
				double candidateHighestScore = candidates.agenda().get(step).get(lastCandidate).currentScore();
				double oracleScore = oracles.agenda().get(step).get(lastOracle).currentScore();
				
				if(Double.compare(oracleScore, candidateHighestScore) >= 0) continue;
				
				double error = Math.max(1.0 - (oracleScore - candidateHighestScore), 0.0);
				
				if(Double.compare(error, errorMax) > 0){
					errorMax = error;
					stepMax = step;
				}
			}
		}
		
		return stepMax;
	}	
	
}
