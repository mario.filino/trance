package trance.matrix;

import org.jblas.FloatMatrix;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.indexing.NDArrayIndex;


public final class StdMatrix {
	
	private StdMatrix(){
		
	}

	public static FloatMatrix getMatrix(FloatMatrix source, int row0, int row1, int col0, int col1){
		return source.getRange(row0, row1 + 1, col0, col1 + 1);
	}
}
