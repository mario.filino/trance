package trance.loss;

import java.util.ArrayList;
import java.util.List;

import trance.progs.State;

public final class HingeLoss {
	private HingeLoss(){
		
	}
	
	public static double loss(List<List<State>> candidate, List<List<State>> oracle, int step){
		double normalize = 0.0;
		double expectedScore = 0.0;
		double loss = 0.0;
		
		double oracleScore = oracle.get(step).get(0).currentScore();
		List<State> selectedAgenda = candidate.get(step);
		
		List<State> subAgenda = new ArrayList<State>();
		
		boolean done = false;
		int i = 0;
		
		// get subset of agenda with derivation score higher than oracleScore
		while(i < selectedAgenda.size() && !done){
			if(Double.compare(selectedAgenda.get(i).currentScore(), oracleScore) > 0){
				subAgenda.add(selectedAgenda.get(i));
				normalize += Math.exp(selectedAgenda.get(i).currentScore());
			}else{
				done = true;
			}
			
			i++;
		}
		
		// compute expected score
		for(State state : subAgenda){
			expectedScore += Math.exp(state.currentScore()) * state.currentScore() / normalize;
		}
		
		loss = Math.max(0.0, 1.0 - oracleScore + expectedScore);
		
		return loss;
	}

}
