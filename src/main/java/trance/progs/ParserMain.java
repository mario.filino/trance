package trance.progs;

import java.util.ArrayList;
import java.util.List;

import trance.model.Model;

/**
 * @author Mario Filino
 *
 */
public class ParserMain {
	private static final String wordVectorPath = "example/vectors.txt";
	private static final String grammarPath = "example/grammar.txt";
	private static final String modelPath = "example/model/";
	
	private static final int unarySize = 3;
	private static final int beam = 32;
	private static final int kbest = 128;
	private static final int hiddenSize = 64;
	private static final int embeddingSize = 1024;
	private static final boolean lowercase = true;
	
	public static void main(String[] args) throws Exception{
		// prepare Symbol
		Symbol symbol = new Symbol(grammarPath);
		
		// prepare Rule
		Grammar grammar = new Grammar(symbol);
		
		// prepare WordVectormodel
		System.out.println("loading word vector...");
		Terminal vec = new Terminal(wordVectorPath);
				
		// prepare Model
		Model param = new Model(hiddenSize, embeddingSize, symbol, vec);
		System.out.println("randomizing model...");
		param.loadModel(modelPath);
		
		Tree trees = new Tree();
		trees.loadTree("example/WSJ-train.treebank");
				
		// prepare InputQueue
		Queue queue = new Queue(trees.sentences().get(0), lowercase, param, vec);
		queue.computeHiddenState();
		
		System.out.println("sentence length: " + trees.sentences().get(0).length);
		
		// prepare TreeStack
		TreeStack stack = new TreeStack(param, symbol);
		
		// prepare Axiom
		State initialState = new State(stack, queue, grammar, unarySize);
		
		long startTimeParser = System.currentTimeMillis();
		
		List<List<State>> candidates = new ArrayList<>();
		
		// Parser
		Parser parser = new Parser(initialState, beam, kbest);
		parser.parse(candidates);
		
		long endTimeParser = System.currentTimeMillis();
		
		System.out.println("Parse Time: " + (endTimeParser - startTimeParser) + " ms, agenda size: " + parser.agenda().size());
		
		System.out.println(candidates.get(candidates.size() - 1));
	}
}
