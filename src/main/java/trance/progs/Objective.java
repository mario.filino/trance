package trance.progs;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import org.jblas.FloatMatrix;

import trance.model.Model;

public class Objective {
	protected Map<State, Backward> backward;
	protected ArrayList<List<State>> states;
	
	protected FloatMatrix[] queue;
	
	public void initialize(Parser candidates, OracleParser oracles){
		backward = new HashMap<State, Backward>();
		
		int capacity = Math.max(candidates.agenda().size(), oracles.agenda().size());
		states = new ArrayList<List<State>>(capacity);
		
		for(int i = 0; i < capacity; i++){
			states.add(i, new ArrayList<State>());
		}
	}
		
	public Backward backwardState(Model param, State state, double loss) throws InterruptedException{
		if(backward.get(state) == null){
			backward.put(state, new Backward());
		}
			
		Backward back = backward.get(state);	
		back.loss += loss;
		
		if(back.delta.rows == 0){
			back.delta = new FloatMatrix(param.hiddenSize(), 1);
		}
		
		return back;
	}
	
	public Backward backwardSate(Model param, State state){
		if(backward.get(state) == null){
			backward.put(state, new Backward());
		}
		
		Backward back = backward.get(state);
		
		if(back.delta.rows == 0){
			back.delta = new FloatMatrix(param.hiddenSize(), 1);
		}
		
		return back;
	}
}
