package trance.progs;

import java.io.IOException;

import edu.stanford.nlp.ling.Label;
import edu.stanford.nlp.ling.StringLabel;
import edu.stanford.nlp.trees.LabeledScoredTreeFactory;
import edu.stanford.nlp.trees.Tree;
import edu.stanford.nlp.trees.TreeFactory;

public class BinarizeRight {
private TreeFactory treeFactory;
	
	public BinarizeRight(TreeFactory tf){
		treeFactory = tf;
	}
	
	public void binarize(Tree source, Tree target){
		if(source.children().length == 0){
			target.setValue(source.label().value());
		}else if(source.children().length <= 2){
			target.setValue(source.label().value());
			
			for(int i = 0; i < source.children().length; i++){
				target.addChild(i, treeFactory.newLeaf("temp"));
				binarize(source.children()[i], target.children()[i]);
			}
		}else{
			target.setValue(source.label().value());
			for(int i = 0; i < 2; i++){
				target.addChild(i, treeFactory.newLeaf("temp"));
			}
			
			int first = 0;
			int last = source.children().length - 1;
			Label newLabel = new StringLabel(source.label().value() + "^");
			
			// right-heavy binarization
			binarize(source.children()[first], target.children()[0]);
			
			binarize(newLabel, source, first + 1, last, target.children()[1]);
		}
	}
	
	public void binarize(Label label, Tree source, int first, int last, Tree target){
		target.setValue(label.value());
		for(int i = 0; i < 2; i++){
			target.addChild(i, treeFactory.newLeaf("temp"));
		}
		
		if(distance(first, last) == 2){
			binarize(source.children()[first], target.children()[0]);
			binarize(source.children()[first + 1], target.children()[1]);
		}else{
			binarize(source.children()[first], target.children()[0]);
			binarize(label, source, first + 1, last, target.children()[1]);
		}
	}
	
	private int distance(int first, int last){
		return last - first + 1;
	}
	
	public static void main(String[] args) throws IOException{
		trance.progs.Tree trees = new trance.progs.Tree();
		trees.loadTree("example/WSJ-train.treebank");
		
		TreeFactory treeFactory = new LabeledScoredTreeFactory();
		
		Tree target = treeFactory.newLeaf("temp");
		Tree source = trees.tree().get(0);
		
		BinarizeRight bl = new BinarizeRight(treeFactory);
		bl.binarize(source, target);
		
		System.out.println(target.pennString());
	}
}
