package trance.progs;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.javatuples.Pair;

public class TestDriver {
	public static void main(String[] args) throws Exception{				
		Tree trees = new Tree();
		trees.loadTree("example/WSJ-22-test.treebank");
		System.out.println(Arrays.asList(trees.sentences().get(0)));
		System.out.println(Arrays.asList(trees.sentences().get(1)));
		
		Pair<Span, Integer> a = new Pair<Span, Integer>(new Span(0, 40), 1);
		System.out.println(a);
		
		Symbol symbol = new Symbol("example/grammar.txt");
		System.out.println(symbol.symbol().get(10));
		
		String test = "[PP^]";
		System.out.println(test.contains("^"));
	}
}
