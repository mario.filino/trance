package trance.progs;

public class TransitionAction implements Comparable<TransitionAction>{
	private int action;
	private int symbol;
	private double score;
	
	public TransitionAction(int action, int symbol, double score){
		this.action = action;
		this.score = score;
		this.symbol = symbol;
	}
	
	public TransitionAction(int action, int symbol){
		this.action = action;
		this.symbol = symbol;
		score = 0.0;
	}
	
	public int action(){
		return action;
	}
	
	public int symbol(){
		return symbol;
	}
	
	public double score(){
		return score;
	}
	
	public String toString(){
		return "[" + action + " " + symbol + " " + score + "]";
	}
	
	public boolean equals(Object o) {
        if (!(o instanceof TransitionAction))
            return false;
        
        TransitionAction n = (TransitionAction) o;
        
        return Double.compare(n.score(), score) == 0;
    }

	public int compareTo(TransitionAction n) {	
        return Double.compare(score, n.score());
	}
}
