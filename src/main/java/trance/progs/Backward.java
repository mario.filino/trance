package trance.progs;

import org.jblas.FloatMatrix;

public class Backward {
	public double loss;
	public FloatMatrix delta;
	
	public Backward(){
		loss = 0.0;
		delta = new FloatMatrix();
	}
}
