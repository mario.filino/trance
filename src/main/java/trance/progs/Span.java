package trance.progs;

public class Span {
	private int first;
	private int last;
	
	/**
	 * Default constructor
	 */
	public Span(){
		this.first = -1;
		this.last = -1;
	}
	
	/**
	 * User-defined constructor
	 * @param first
	 * @param last
	 */
	public Span(int first, int last){
		this.first = first;
		this.last = last;
	}
	
	/**
	 * Return true is span is empty
	 * @return
	 */
	public boolean empty(){
		return this.first == this.last;
	}
	
	/**
	 * Size of span
	 * @return
	 */
	public int size(){
		return this.last - this.first;
	}
	
	/**
	 * Return first
	 * @return
	 */
	public int first(){
		return this.first;
	}
	
	/**
	 * Return last 
	 * @return
	 */
	public int last(){
		return this.last;
	}
	
	/**
	 * @return String representation of span
	 */
	public String toString(){
		return this.first + ".." + this.last;
	}
}
