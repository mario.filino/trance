package trance.progs;

import java.util.Random;

import trance.model.Model;
import trance.objective.ViolationMax;
import trance.optimize.Adadec;

public class Learn {
	private static final String wordVectorPath = "example/terminal.txt";
	private static final String grammarPath = "example/grammar.txt";
	private static final String modelPath = "example/model/";
	private static final String trainingFilePath = "example/WSJ-train.treebank";
	private static final String testFilePath = "example/WSJ-22.treebank";
	
	private static final int unarySize = 3;
	private static final int beam = 32;
	private static final int kbest = 100;
	private static final int hiddenSize = 64;
	private static final int embeddingSize = 1024;
	private static final boolean lowercase = true;
	
	private static final int batchSize = 4;
	private static final int iteration = 10;
	private static final double eta = 1e-2;
	private static final double gamma = 0.9;
	private static final double epsilon = 1;
	private static final double lambda = 1e-5;
	
	private static final boolean leftBinarize = false;
	
	private static final int debugLevel = 0;
	
	public static void main(String[] args) throws Exception{
		// prepare Symbol
		Symbol symbol = new Symbol(grammarPath);
		
		// prepare Rule
		Grammar grammar = new Grammar(symbol);
		
		// prepare WordVectormodel
		System.out.println("loading word vector...");
		Terminal vec = new Terminal(wordVectorPath);
				
		// prepare Model
		Model param = new Model(hiddenSize, embeddingSize, symbol, vec);
		System.out.println("randomizing model...");
		param.randomize();
		
		// prepate Training trees
		Tree trees = new Tree();
		trees.loadTree(trainingFilePath);
		System.out.println("training data: " + trees.size());
		
		// prepare Test trees
		Tree tests = new Tree();
		tests.loadTree(testFilePath);
		System.out.println("test data: " + tests.size());
		
		System.out.print("binary: " + grammar.binary().values().size());
		System.out.print(" unary: " + grammar.unary().values().size());
		System.out.print(" preterminal: " + grammar.preterminal().values().size());
		System.out.println(" terminal: " + grammar.preterminal().keySet().size());
		
		// prepare oracles
		Oracle[] oracles = new Oracle[trees.size()];
		for(int i = 0; i < oracles.length; i++){
			Oracle oracle = new Oracle(symbol);
			oracle.assign(trees.tree().get(i), leftBinarize);
			oracles[i] = oracle;
		}
		
		// Violation-Max
		ViolationMax vm = new ViolationMax();
				
		// Adadec
		Adadec adadec = new Adadec(param, lambda, eta, epsilon, gamma);

		for(int i = 0; i < iteration; i++){
			// shuffle training data
			//Learn.shuffleArray(oracles);
			
			System.out.println("iteration: " + (i + 1));
			
			trance.gradient.Model grad = new trance.gradient.Model(hiddenSize, embeddingSize, symbol);
						
			int batch = (int) Math.floor((double) trees.size() / (double) batchSize);
			int parsed = 0;
			int instances = 0;
			double loss = 0.0;
			double count = 0.0;
			
			for(int batchId = 0; batchId < batch; batchId++){
				grad.setZero();
				
				System.out.println("batch: " + batchId);
				
				int first = batchId * batchSize;
				int last = first + batchSize;
								
				for(int id = first; id < last; id++){					
					Oracle oracle = oracles[id];
					
					// prepare Queue
					Queue queue = new Queue(oracle.sentence(), lowercase, param, vec);
					queue.computeHiddenState();

					// prepare Stack
					TreeStack stack = new TreeStack(param, symbol);
					
					// prepare Initial State
					State initialState = new State(stack, queue, grammar, unarySize);
					
					long startTimeParser = System.currentTimeMillis();
					
					// Parser
					Parser parser = new Parser(initialState, beam, kbest);
					parser.parse();
					
					long endTimeParser = System.currentTimeMillis();
										
					if(debugLevel >= 2)
						System.out.println("Parse Time: " + (endTimeParser - startTimeParser) + " ms, agenda size: " + parser.agenda().size());
					
					long startTimeOracleParser = System.currentTimeMillis();
					
					// Oracle Parser
					OracleParser op = new OracleParser(initialState, oracle);
					op.parse();
					
					System.out.println("oracle score: " + op.agenda().get(op.agenda().size() - 1).get(0).currentScore());
					
					long endTimeOracleParser = System.currentTimeMillis();
					
					if(debugLevel >= 2)
						System.out.println("Oracle Parse Time: " + (endTimeOracleParser - startTimeOracleParser) + " ms, agenda size: " + op.agenda().size());
					
					if(!parser.agenda().isEmpty())
						parsed++;
					instances++;
					
					double lossTemp = vm.execute(param, parser, op, grad);
					
					if(debugLevel >= 1)
						System.out.println("tree " + id + "'s loss: " + lossTemp);
					
					loss += lossTemp;
					count++;
				}
				
				if(debugLevel >= 1)
					System.out.println("batch-" + batchId + "'s loss: " + loss / count + "\n");
								
				adadec.update(param, grad);	
			}

			System.out.println("\nloss: " + loss / count);
			System.out.println("instances: " + instances);
			System.out.println("parsed: " + parsed + "\n");
		}
		
		param.saveModel(modelPath);
	}
	
	public static void shuffleArray(Oracle[] array){
		Oracle temp;
	    int index;
	    Random random = new Random();
	    
	    for (int i = array.length - 1; i > 0; i--){
	        index = random.nextInt(i + 1);
	        if (index != i){
	        	 index = random.nextInt(i + 1);
	             temp = array[index];
	             array[index] = array[i];
	             array[i] = temp;
	        }
	    }
	}
}
