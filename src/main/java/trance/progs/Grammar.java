package trance.progs;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.ListMultimap;

/**
 * @author Mario Filino
 *
 */
public class Grammar {
	private String path;
	
	private Integer goal;
	private Integer sentence;
	private Integer sentenceBinarized;
	private ListMultimap<Integer, Integer> unary;
	private ListMultimap<List<Integer>, Integer> binary;
	private ListMultimap<String, Integer> preterminal;

	private Symbol symbol;
	
	public Grammar(Symbol symbol) throws IOException{
		goal = null;
		sentence = null;
		sentenceBinarized = null;
		
		unary = ArrayListMultimap.create();
		binary = ArrayListMultimap.create();
		preterminal = ArrayListMultimap.create();

		this.path = symbol.path();
		this.symbol = symbol;
		generateRule(this.path);
	}
	
	private boolean isGoal(String grammar){
		return grammar.matches("\\[.*\\]");
	}
	
	private boolean isUnary(String grammar){
		return grammar.matches("\\[.*\\] -> \\[[^(\\]|\\[)]*\\] \\|.*");
	}
	
	private boolean isBinary(String grammar){
		return grammar.matches("\\[.*\\] -> \\[[^(\\]|\\[)]*\\] \\[[^(\\]|\\[)]*\\].*");
	}
	
	private boolean isPreterminal(String grammar){
		return grammar.matches("\\[.*\\] -> [^(\\[)]*");
	}
	
	private void generateRule(String path) throws IOException{
		File file = new File(path);
		
		BufferedReader reader = new BufferedReader(new FileReader(file));
		try {
			String line;
			
			while((line = reader.readLine()) != null){
				if(line.isEmpty()){
					continue;
				}
				
				String[] grammar = line.split(" ");
				
				if(isGoal(line)){
					if(goal != null && sentence != null)
						throw new RuntimeException("Multiple goals/ sentences supported");
					
					if(goal == null){
						goal = this.symbol.index().get(grammar[0]);
					}else if(sentence == null){
						sentence = this.symbol.index().get(grammar[0]);
						sentenceBinarized = this.symbol.index().get("[" + grammar[0].replaceAll("\\[", "").replaceAll("\\]", "") + "^]");
					}
						
				}else{
					if(isUnary(line)){
						int rhs = this.symbol.index().get(grammar[2]);
						int lhs = this.symbol.index().get(grammar[0]);
						
						unary.put(rhs, lhs);
					}else{
						if(isBinary(line)){
							int rhsFront = this.symbol.index().get(grammar[2]);
							int rhsBack = this.symbol.index().get(grammar[3]);
							int lhs = this.symbol.index().get(grammar[0]);
							
							List<Integer> temp = new ArrayList<Integer>();
							temp.add(rhsFront);
							temp.add(rhsBack);
							
							binary.put(temp, lhs);
						}else{
							if(isPreterminal(line)){
								String rhs = grammar[2];
								int lhs = this.symbol.index().get(grammar[0]);
								
								preterminal.put(rhs, lhs);
							}
						}
					}
				}
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			reader.close();
		}
	}
	
	public int goal(){
		return goal;
	}
	
	public int sentence(){
		return sentence;
	}
	
	public int sentenceBinarized(){
		return sentenceBinarized;
	}
	
	public List<Integer> possibleUnary(int rhs){
		return unary.get(rhs);
	}
	
	public List<Integer> possibleBinary(int rhsFront, int rhsBack){
		List<Integer> temp = new ArrayList<Integer>();
		temp.add(rhsFront);
		temp.add(rhsBack);
		
		return binary.get(temp);
	}
		
	public List<Integer> possiblePreterminal(String word){
		List<Integer> possible = preterminal.get(word);
		
		if(possible.isEmpty()){
			possible = preterminal.get(symbol.signature(word));
		}
		
		return possible;
	}
	
	public ListMultimap<Integer, Integer> unary(){
		return unary;
	}
	
	public ListMultimap<List<Integer>, Integer> binary(){
		return binary;
	}
	
	public ListMultimap<String, Integer> preterminal(){
		return preterminal;
	}
}
