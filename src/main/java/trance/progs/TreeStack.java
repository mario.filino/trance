package trance.progs;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.jblas.FloatMatrix;

import trance.exceptions.HiddenVectorSizeException;
import trance.model.Model;

/**
 * @author Mario Filino
 *
 */
public class TreeStack {
	private int hiddenDimSize;
	private int currentIndex;
	
	private FloatMatrix initialHiddenState;
	private List<FloatMatrix> stack;
	
	private List<Integer> label;
	
	private Model param;
	private Symbol symbol;
	
	/**
	 * Constructor
	 * 
	 * @param hidden
	 * @param hiddenState
	 * @throws HiddenVectorSizeException
	 */
	public TreeStack(Model param, Symbol grammar) throws HiddenVectorSizeException{
		hiddenDimSize = param.hiddenSize();
		currentIndex = 0;
		
		stack = new ArrayList<FloatMatrix>();
		label = new ArrayList<Integer>();
		
		this.param = param;
		
		initialHiddenState = new FloatMatrix(hiddenDimSize, 1);
		initHiddenState(this.param.getBa());
		
		this.symbol = grammar;
	}
	
	/**
	 * Copy constructor
	 * 
	 * @param n
	 */
	public TreeStack(TreeStack n){
		this.hiddenDimSize = n.hiddenDimSize;
		this.currentIndex = n.currentIndex;
		this.initialHiddenState = n.initialHiddenState.getColumns(new int[] {0});
		
		this.stack = new ArrayList<FloatMatrix>();
		for(FloatMatrix array : n.stack){
			this.stack.add(array.getColumn(0));
		}
		
		this.label = new ArrayList<Integer>(n.label);
		this.param = n.param;
		this.symbol = n.symbol;
	}
	
	@Override
	public String toString(){
		String output;
		
		if(isEmpty()){
			output = "[]";
		}else{
			output = "[ ";
			for(int i = 1; i <= currentSize(); i++){
				output += symbol.symbol().get(label.get(i));
				output += " ";
			}
			output += "]";
		}
		
		return output;
	}
	
	public void initHiddenState(FloatMatrix hiddenState) throws HiddenVectorSizeException{
		if(hiddenState.rows != initialHiddenState.rows || hiddenState.columns != initialHiddenState.columns)
            throw new HiddenVectorSizeException("Hidden vector size doesn't match initialized hidden dimension size");

		initialHiddenState = hiddenState;
		stack.add(0, Model.activation(initialHiddenState));
		label.add(0, 0);
	}
	
	public void initHiddenState(String path) throws IOException, HiddenVectorSizeException{
		File hiddenStateFile = new File(path);
    	
    	BufferedReader reader = new BufferedReader(new FileReader(hiddenStateFile));
    	String line = reader.readLine();
    	String[] initial = line.split(" ");
    	
    	int hiddenStateSize = Integer.parseInt(initial[0]);
    	
    	float[] buffer = new float[hiddenStateSize]; 	
    	int i = 0;
    	
    	while((line = reader.readLine()) != null){	
    		if(line.isEmpty()){
    			continue;
    		}
    		
    		float value = Float.parseFloat(line);
    		buffer[i] = value;
    		i++;
    	}
    	
    	reader.close();
    	
    	FloatMatrix hiddenState = new FloatMatrix(buffer);
    	initHiddenState(hiddenState);
	}
	
	public static FloatMatrix readHiddenState(String path) throws IOException, HiddenVectorSizeException{
		File hiddenStateFile = new File(path);
    	
    	BufferedReader reader = new BufferedReader(new FileReader(hiddenStateFile));
    	String line = reader.readLine();
    	String[] initial = line.split(" ");
    	
    	int hiddenStateSize = Integer.parseInt(initial[0]);
    	
    	float[] buffer = new float[hiddenStateSize]; 	
    	int i = 0;
    	
    	while((line = reader.readLine()) != null){	
    		if(line.isEmpty()){
    			continue;
    		}
    		
    		float value = Float.parseFloat(line);
    		buffer[i] = value;
    		i++;
    	}
    	
    	reader.close();
    	
    	FloatMatrix hiddenState = new FloatMatrix(buffer);
    	
    	return hiddenState;
	}
	
	public FloatMatrix peekHead(){
		return stack.get(currentIndex);
	}
		
	public FloatMatrix peekNext(){
		if(currentIndex < 1)
			throw new IndexOutOfBoundsException("Stack is empty");
		
		return stack.get(currentIndex - 1);
	}
	
	public FloatMatrix peekNextNext(){
		if(currentIndex < 2)
			throw new IndexOutOfBoundsException("Insufficient stack item to do action");
		
		return stack.get(currentIndex - 2);
	}
	
	public void push(FloatMatrix newItem, int symbol) throws HiddenVectorSizeException{
		if(newItem.rows != hiddenDimSize || newItem.columns != 1)
			throw new HiddenVectorSizeException("Stack element size doesn't match initialized hidden dimension size");

		currentIndex++;
		stack.add(currentIndex, newItem);
		label.add(currentIndex, symbol);
	}
	
	public FloatMatrix pop(){
		if(currentIndex < 1)
			throw new IndexOutOfBoundsException("Stack is empty");

		FloatMatrix item = stack.remove(currentIndex);
		label.remove(currentIndex);
		
		currentIndex--;
		
		return item;
	}
	
	public int peekHeadLabel(){
		if(currentIndex < 1)
			throw new IndexOutOfBoundsException("Stack is empty");
		
		return label.get(currentIndex);
	}
	
	public int peekNextLabel(){
		if(currentIndex < 2)
			throw new IndexOutOfBoundsException("String only has one item");
		
		return label.get(currentIndex - 1);
	}
	
	public int currentSize(){
		return currentIndex;
	}
	
	public boolean isEmpty(){
		return currentIndex < 1;
	}
	
	public int hiddenStateSize(){
		return hiddenDimSize;
	}
	
	public Model param(){
		return param;
	}
	
	public Symbol symbol(){
		return symbol;
	}
}
