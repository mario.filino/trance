	package trance.progs;

import java.util.ArrayList;
import java.util.List;

import org.jblas.FloatMatrix;

import trance.exceptions.HiddenVectorSizeException;
import trance.model.Model;

/**
 * @author Mario Filino
 *
 */
public class State implements Comparable<State>{
	private int hidden;
	private int embedding;
	private int consecutiveUnary;
	private int unaryCounter;
	private int maxConsUnary;
	private int maxUnary;
	private double currentScore;
	private boolean parsingFlag;
	private List<TransitionAction> actionSequence;
	
	private TreeStack stack;
	private Queue queue;
	private Model param;
	private Grammar grammar;
	private Symbol symbol;
	private Span span;
	
	private int next;
	private int step;
	private int operation;
	private int label;
	private String head;
	private int queueIndex;
	
	private State derivation_;
	private State stack_;
	private State reduced_;
	
	/**
	 * Constructor
	 * 
	 * @param stack
	 * @param param
	 * @param queue
	 * @param grammar
	 * @param maxUnary
	 */
	public State(TreeStack stack, Queue queue, Grammar grammar, int maxConsUnary){
		this.stack = new TreeStack(stack);
		this.queue = new Queue(queue);
		this.param = stack.param();
		this.grammar = grammar;
		this.maxConsUnary = maxConsUnary;
		
		maxUnary = maxConsUnary * queue.size();
		unaryCounter = 0;
		consecutiveUnary = 0;
		currentScore = 0;
		parsingFlag = false;
		actionSequence = new ArrayList<TransitionAction>();
		
		hidden = param.hiddenSize();
		embedding = param.embeddingSize();
		
		this.symbol = stack.symbol();
		this.span = new Span(-1, 0);
		
		next = 0;
		step = 0;
		operation = Operation.AXIOM;		
		label = symbol.axiom_();
		head = Symbol.epsilon_();
		queueIndex = -1;
		
		derivation_ = null;
		stack_ = null;
		reduced_ = null;
	}
	
	/**
	 * Copy constructor
	 * 
	 * @param n
	 */
	public State(State n){
		this.hidden = n.hidden;
		this.embedding = n.embedding;
		this.consecutiveUnary = n.consecutiveUnary;
		this.unaryCounter = n.unaryCounter;
		this.maxConsUnary = n.maxConsUnary;
		this.maxUnary = n.maxUnary;
		this.currentScore = n.currentScore;
		this.parsingFlag = n.parsingFlag;
		this.actionSequence = n.actionSequence();
		this.stack = new TreeStack(n.stack);
		this.queue = new Queue(n.queue);
		this.param = n.param;
		this.grammar = n.grammar;
		this.symbol = n.symbol;
		this.span = n.span;
		this.step = n.step;
		this.operation = n.operation;
		this.label = n.label;
		this.head = n.head;
		this.queueIndex = n.queueIndex;
		this.derivation_ = n.derivation_;
		this.stack_ = n.stack_;
		this.reduced_ = n.reduced_;
		this.next = n.next;
	}
	
	@Override
	public String toString(){
		return "Stack: " + stack + " Queue: " + queue + " Score: " + currentScore + " Step: " + step;
	}
	
	public int compareTo(State o) {
		return Double.compare(this.currentScore, o.currentScore);
	}
	
	public boolean equals(Object o) {
        if (!(o instanceof State))
            return false;
        
        State n = (State) o;
        
        return Double.compare(n.currentScore(), this.currentScore) == 0;
    }
	
	/**
	 * Returns list of all possible action. 1 for shift, 2 for reduce, 3 for unary, 4 for finish, 5 for idle
	 * 
	 * @return List<Integer>
	 */
	public List<Integer> possibleActions(){
		List<Integer> actions = new ArrayList<Integer>();
		
		if(parsingFlag){
			actions.add(Operation.IDLE);
		}else{
			if(stack.isEmpty()){
				if(!queue.isEmpty()){
					actions.add(Operation.SHIFT);
				}
			}else{
				if(isRootReached()){
					if(queue.isEmpty() && stack.currentSize() < 2){
						actions.add(Operation.FINAL);
					}
				}else{
					if(!queue.isEmpty()){
						actions.add(Operation.SHIFT);
					}
					
					if(stack.currentSize() >= 2){
						actions.add(Operation.REDUCE);
					}
					
					if(stack.currentSize() >= 1 && consecutiveUnary <= maxConsUnary && unaryCounter <= maxUnary){
						actions.add(Operation.UNARY);
					}
				}
			}
		}
				
		return actions;
	}
	
	public List<Integer> possibleAlternateActions(){
		List<Integer> actions = new ArrayList<Integer>();
		
		if(parsingFlag){
			actions.add(Operation.IDLE);
		}else{
			if(stack.isEmpty()){
				if(!queue.isEmpty()){
					actions.add(Operation.SHIFT);
				}
			}else{
				if(isRootReached()){
					if(queue.isEmpty() && stack.currentSize() < 2){
						actions.add(Operation.FINAL);
					}
				}else{
					if(!queue.isEmpty()){
						actions.add(Operation.SHIFT);
					}
					
					if(stack.currentSize() >= 2){
						actions.add(Operation.REDUCE);
					}
					
					if(stack.currentSize() == 1){
						actions.add(Operation.UNARY);
					}
				}
			}
		}
				
		return actions;
	}
		
	public List<Integer> possibleSymbolsShift(){
		return grammar.possiblePreterminal(queue.currentWord());
	}
	
	public List<Integer> possibleSymbolsReduce(){
		return grammar.possibleBinary(stack.peekNextLabel(), stack.peekHeadLabel());
	}
	
	public List<Integer> possibleSymbolsUnary(){
		return grammar.possibleUnary(stack.peekHeadLabel());
	}
	
	
	/**
	 * Generate list of k-best possible transition actions
	 * 
	 * @param kbest
	 * @return List<TransactionAction>
	 * @throws HiddenVectorSizeException 
	 */
	public List<TransitionAction> legalMoves(int kbest) throws Exception{
		List<TransitionAction> actions = new ArrayList<TransitionAction>();
		List<Integer> possibleAct = possibleActions();
		
		for(int value : possibleAct){
			if(value == Operation.SHIFT){
				List<Integer> symbol = possibleSymbolsShift();
				for(int sym : symbol){
					actions.add(new TransitionAction(value, sym, shiftScore(sym) + currentScore));
				}
			}
			
			if(value == Operation.REDUCE){
				List<Integer> symbol = possibleSymbolsReduce();
				for(int sym : symbol){
					if(!((stack.currentSize() > 2 || !queue.isEmpty()) && (isSymbolGoal(sym) || isSymbolSentence(sym)))){
						actions.add(new TransitionAction(value, sym, reduceScore(sym) + currentScore));
					}
				}
			}
			
			if(value == Operation.UNARY){
				List<Integer> symbol = possibleSymbolsUnary();
				for(int sym : symbol){
					if(!((stack.currentSize() > 1 || !queue.isEmpty()) && (isSymbolGoal(sym) || isSymbolSentence(sym)))){
						actions.add(new TransitionAction(value, sym, unaryScore(sym) + currentScore));
					}
				}
			}

			if(value == Operation.FINAL){
				actions.add(new TransitionAction(value, symbol.final_(), finishScore() + currentScore));
			}

			if(value == Operation.IDLE){
				actions.add(new TransitionAction(value, symbol.idle_(), idleScore() + currentScore));
			}
		}

		if(actions.size() < kbest){
			return actions;
		}else{
			return com.google.common.collect.Ordering.natural().greatestOf(actions, kbest);
		}
	}
	
	/**
	 * Generate list of all possible transition actions
	 * 
	 * @param kbest
	 * @return List<TransactionAction>
	 * @throws HiddenVectorSizeException 
	 */
	public List<TransitionAction> legalMoves() throws Exception{
		List<TransitionAction> actions = new ArrayList<TransitionAction>();
		List<Integer> possibleAct = possibleActions();
		
		for(int value : possibleAct){
			if(value == Operation.SHIFT){
				List<Integer> symbol = possibleSymbolsShift();
				for(int sym : symbol){
					actions.add(new TransitionAction(value, sym, shiftScore(sym) + currentScore));
				}
			}
			
			if(value == Operation.REDUCE){
				List<Integer> symbol = possibleSymbolsReduce();
				for(int sym : symbol){
					if(!((stack.currentSize() > 2 || !queue.isEmpty()) && (isSymbolGoal(sym) || isSymbolSentence(sym)))){
						actions.add(new TransitionAction(value, sym, reduceScore(sym) + currentScore));
					}
				}
			}
			
			if(value == Operation.UNARY){
				List<Integer> symbol = possibleSymbolsUnary();
				for(int sym : symbol){
					if(!((stack.currentSize() > 1 || !queue.isEmpty()) && (isSymbolGoal(sym) || isSymbolSentence(sym)))){
						actions.add(new TransitionAction(value, sym, unaryScore(sym) + currentScore));
					}
				}
			}

			if(value == Operation.FINAL){
				actions.add(new TransitionAction(value, symbol.final_(), finishScore() + currentScore));
			}

			if(value == Operation.IDLE){
				actions.add(new TransitionAction(value, symbol.idle_(), idleScore() + currentScore));
			}
		}
				
		return actions;
	}
	
	/**
	 * Shift action of shift-reduce parsing, set update to true to update stack and queue
	 * 
	 * @param index
	 * @param update
	 * @return INDArray
	 * @throws HiddenVectorSizeException
	 * @throws InterruptedException 
	 */
	public FloatMatrix shift(int index, boolean update) throws Exception{
		if(queue.isEmpty())
			throw new IllegalArgumentException("Empty queue, shift action invalid");

		if(parsingFlag)
			throw new IllegalArgumentException("parsing has finished, shift action invalid");
		
		if(!stack.isEmpty() && isRootReached())
			throw new IllegalArgumentException("Root has been reached, shift action invalid");

		FloatMatrix temp = new FloatMatrix(hidden, 1);
		
		temp.addi(param.getHsh(index).mmul(stack.peekHead()));
		temp.addi(param.getQsh(index).mmul(queue.peek().getHiddenState()));
		temp.addi(param.getWsh(index).mmul(queue.peek().getWordVector()));
		temp.addi(param.getBsh(index));
		
		Model.activation(temp);
					
		if(update){
			double score = (double) param.getVsh(index).mmul(temp).get(0, 0) + (double) param.getvsh(index);
			
			//System.out.println("shift score: " + score);
			
			currentScore += score;
			step++;
			operation = Operation.SHIFT;
			label = index;
			head = queue.currentWord();
			
			queueIndex = queue.currentIndex();
			next++;
			
			actionSequence.add(new TransitionAction(operation, index, currentScore));
			queue.remove();
			stack.push(temp, index);
			consecutiveUnary = 0;
		}
		
		return temp;
	}
	
	public double shiftScore(int symbol) throws Exception{
		Double temp = (double) param.getVsh(symbol).mmul(shift(symbol, false)).get(0, 0);
		
		return temp + (double) param.getvsh(symbol);
	}
	
	/**
	 * Reduce action of shift-reduce parsing, set update to true to update stack and queue
	 * 
	 * @param index
	 * @param update
	 * @return INDArray
	 * @throws HiddenVectorSizeException
	 */
	public FloatMatrix reduce(int index, boolean update) throws HiddenVectorSizeException{
		if(stack.currentSize() < 2)
			throw new IllegalArgumentException("Insufficient stack item, reduce action invalid");

		if(parsingFlag)
			throw new IllegalArgumentException("parsing has finished, reduce action invalid");
		
		if(!stack.isEmpty() && isRootReached())
			throw new IllegalArgumentException("Root has been reached, reduce action invalid");
		
		/*if((stack.currentSize() > 2 || !queue.isEmpty()) && (isSymbolGoal(index) || isSymbolSentence(index)))
			throw new IllegalArgumentException("Not all words consumed or stack size > 2, reduce with symbol '" + index + "' invalid");*/

		FloatMatrix temp = new FloatMatrix(hidden, 1);

		temp.addi(param.getHre(index).mmul(stack.peekNextNext()));
		temp.addi(param.getQre(index).mmul(queue.peek().getHiddenState()));
		temp.addi(param.getWre0(index).mmul(stack.peekHead()));
		temp.addi(param.getWre1(index).mmul(stack.peekNext()));
		temp.addi(param.getBre(index));
		
		Model.activation(temp);
				
		if(update){
			double score = (double) param.getVre(index).mmul(temp).get(0, 0) + (double) param.getvre(index);
			
			//System.out.println("reduce score: " + score);
			
			currentScore += score;
			step++;
			operation = Operation.REDUCE;
			label = index;
			head = Symbol.epsilon_();
			
			queueIndex = queue.currentIndex();
			
			actionSequence.add(new TransitionAction(operation, index, currentScore));
			stack.pop();
			stack.pop();
			stack.push(temp, index);
			consecutiveUnary = 0;
		}
		
		return temp;
	}
	
	public double reduceScore(int symbol) throws HiddenVectorSizeException{
		Double temp = (double) param.getVre(symbol).mmul(reduce(symbol, false)).get(0, 0);
		
		return temp + (double) param.getvre(symbol);
	}
	
	/**
	 * Unary action of shift-reduce parsing, set update to true to update stack and queue
	 * 
	 * @param index
	 * @param update
	 * @return INDArray
	 * @throws HiddenVectorSizeException
	 */
	public FloatMatrix unary(int index, boolean update) throws HiddenVectorSizeException{
		if(stack.currentSize() < 1)
			throw new IllegalArgumentException("Insufficient stack item, unary action invalid");

		if(parsingFlag)
			throw new IllegalArgumentException("parsing has finished, unary action invalid");
		
		if(!stack.isEmpty() && isRootReached())
			throw new IllegalArgumentException("Root has been reached, unary action invalid");
		
		/*if((stack.currentSize() > 1 || !queue.isEmpty()) && (isSymbolGoal(index) || isSymbolSentence(index)))
			throw new IllegalArgumentException("Not all words consumed or stack size > 1, unary with symbol '" + index + "' invalid");*/
		
		FloatMatrix temp = new FloatMatrix(hidden, 1);
		
		temp.addi(param.getHun(index).mmul(stack.peekNext()));
		temp.addi(param.getQun(index).mmul(queue.peek().getHiddenState()));
		temp.addi(param.getWun(index).mmul(stack.peekHead()));
		temp.addi(param.getBun(index));
		
		Model.activation(temp);
				
		if(update){
			double score = (double) param.getVun(index).mmul(temp).get(0, 0) + (double) param.getvun(index);
			
			//System.out.println("unary score: " + score);
			
			currentScore += score;
			step++;
			operation = Operation.UNARY;
			label = index;
			head = Symbol.epsilon_();
			
			queueIndex = queue.currentIndex();
			
			actionSequence.add(new TransitionAction(operation, index, currentScore));
			stack.pop();
			stack.push(temp, index);
			consecutiveUnary++;
			unaryCounter++;
		}
		
		return temp;
	}
	
	public double unaryScore(int symbol) throws HiddenVectorSizeException{
		Double temp = (double) param.getVun(symbol).mmul(unary(symbol, false)).get(0, 0);
		
		return temp + (double) param.getvun(symbol);
	}
	
	public FloatMatrix finish(boolean update) throws HiddenVectorSizeException{
		if(parsingFlag)
			throw new IllegalArgumentException("parsing has finished, finish action invalid");
		
		if(!isRootReached())
			throw new IllegalArgumentException("Root hasn't been reached, finish action invalid");
				
		FloatMatrix temp = new FloatMatrix(hidden, 1);
		
		temp.addi(param.getWf().mmul(stack.peekHead()));
		temp.addi(param.getBf());
		
		Model.activation(temp);
				
		if(update){
			double score = (double) param.getVun(symbol.final_()).mmul(temp).get(0, 0) + (double) param.getvun(symbol.final_());
			
			//System.out.println("finish score: " + score);
			
			currentScore += score;
			step++;
			operation = Operation.FINAL;
			label = symbol.final_();
			head = Symbol.epsilon_();
			
			queueIndex = queue.currentIndex();
						
			actionSequence.add(new TransitionAction(operation, symbol.final_(), currentScore));
			parsingFlag = true;
			stack.pop();
			stack.push(temp, symbol.final_());
			
			consecutiveUnary = 0;
		}
		
		return temp;
	}
	
	public double finishScore() throws HiddenVectorSizeException{
		Double temp = (double) param.getVun(symbol.final_()).mmul(finish(false)).get(0, 0);
		
		return temp + (double) param.getvun(symbol.final_());
	}
	
	public FloatMatrix idle(boolean update) throws HiddenVectorSizeException{
		if(!parsingFlag)
			throw new IllegalArgumentException("parsing not finish, idle action invalid");
		
		FloatMatrix temp = new FloatMatrix(hidden, 1);
		
		temp.addi(param.getWi().mmul(stack.peekHead()));
		temp.addi(param.getBi());
		
		Model.activation(temp);
		
		if(update){
			double score = (double) param.getVun(symbol.idle_()).mmul(temp).get(0, 0) + (double) param.getvun(symbol.idle_());
			
			//System.out.println("idle score: " + score);
			
			currentScore += score;
			step++;
			operation = Operation.IDLE;
			label = symbol.idle_();
			head = Symbol.epsilon_();
			
			queueIndex = queue.currentIndex();
						
			actionSequence.add(new TransitionAction(operation, symbol.idle_(), currentScore));
			stack.pop();
			stack.push(temp, symbol.idle_());
			
			consecutiveUnary = 0;
		}
		
		return temp;
	}
	
	public double idleScore() throws HiddenVectorSizeException{
		Double temp = (double) param.getVun(symbol.idle_()).mmul(idle(false)).get(0, 0);
		
		return temp + (double) param.getvun(symbol.idle_());
	}
	
	public int maxConsUnary(){
		return maxConsUnary;
	}
	
	public int maxUnary(){
		return maxUnary;
	}
	
	public Grammar grammar(){
		return grammar;
	}
	
	public int queueSize(){
		return queue.size();
	}
	
	public int stackSize(){
		return stack.currentSize();
	}
	
	public boolean isQueueEmpty(){
		return queue.isEmpty();
	}
	
	public boolean isStackEmpty(){
		return stack.isEmpty();
	}
	
	public int currentConsUnary(){
		return consecutiveUnary;
	}
	
	public int currentUnaryCount(){
		return unaryCounter;
	}
	
	public double currentScore(){
		return currentScore;
	}
	
	public boolean isParsingFinish(){
		return parsingFlag;
	}
	
	public int currentHead(){
		return stack.peekHeadLabel();
	}
	
	public int currentNext(){
		return stack.peekNextLabel();
	}
	
	public boolean isRootReached(){
		return isSymbolGoal(currentHead());
	}
	
	public List<TransitionAction> actionSequence(){
		List<TransitionAction> temp = new ArrayList<TransitionAction>();
		
		for(TransitionAction elem : actionSequence){
			temp.add(new TransitionAction(elem.action(), elem.symbol(), elem.score()));
		}
		
		return temp;
	}
	
	public boolean isSymbolGoal(int symbol){
		return symbol == grammar.goal();
	}
	
	public boolean isSymbolSentence(int symbol){
		return symbol == grammar.sentence();
	}
	
	public State derivation(){
		return derivation_;
	}
	
	public State stack(){
		return stack_;
	}
	
	public State reduced(){
		return reduced_;
	}
	
	public void setDerivation(State state){
		derivation_ = state;
	}
	
	public void setStack(State state){
		stack_ = state;
	}
	
	public void setReduced(State state){
		reduced_ = state;
	}
	
	public int step(){
		return step;
	}
	
	public int hidden(){
		return hidden;
	}
	
	public int label(){
		return label;
	}
	
	public int operation(){
		return operation;
	}
	
	public String head(){
		return head;
	}
	
	public int queueIndex(){
		return queueIndex;
	}
	
	public Queue queue(){
		return queue;
	}
	
	public FloatMatrix layer(){
		return stack.peekHead();
	}
	
	public Span span(){
		return this.span;
	}
	
	public void setSpan(Span span){
		this.span = span;
	}
	
	public int next(){
		return this.next;
	}
	
	public void setNext(int next){
		this.next = next;
	}
}
