package trance.progs;

public class Operation {
	public static final int AXIOM = 0;
	public static final int SHIFT = 1;
	public static final int REDUCE = 2;
	public static final int UNARY = 3;
	public static final int FINAL = 4;
	public static final int IDLE = 5;
	
	private Operation(){
		
	}
}
