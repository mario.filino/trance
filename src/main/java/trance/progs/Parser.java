package trance.progs;

import java.util.ArrayList;
import java.util.List;

import jersey.repackaged.com.google.common.collect.Lists;
import trance.exceptions.HiddenVectorSizeException;

public class Parser {
	private State initialState;
	private int beamSize;
	private int kbest;
	private List<State> currentAgenda;
	private List<List<State>> agenda;

	/**
	 * Constructor
	 * @param state Parser state
	 * @param beam Beam size, used for beam search
	 * @param kbest Kbest size, used for beam search
	 */
	public Parser(State state, int beam, int kbest){
		this.initialState = new State(state);
		this.beamSize = beam;
		this.kbest = kbest;
		this.currentAgenda = new ArrayList<State>();
		this.agenda = new ArrayList<List<State>>();
		initAgenda();
	}
	
	/**
	 * Initialize agenda for beam search
	 */
	private void initAgenda(){
		currentAgenda.add(initialState);
		agenda.add(currentAgenda);
	}
		
	/**
	 * Expand next possible state from current agenda
	 * @throws Exception
	 */
	public void nextAgenda() throws Exception{
		List<State> temp = new ArrayList<State>();
									
		for(State state : currentAgenda){
			
			List<Integer> actions = state.possibleActions();
			
			for(int entry : actions){
				if(entry == Operation.SHIFT){
					List<Integer> symbol = state.possibleSymbolsShift();
					
					for(int sym : symbol){
						State newState = new State(state);
												
						newState.shift(sym, true);
						newState.setDerivation(state);
						newState.setStack(state);
						newState.setReduced(null);
						newState.setSpan(new Span(state.next(), state.next() + 1));
						
						temp.add(newState);
					}
				}else if(entry == Operation.REDUCE){
					List<Integer> symbol = state.possibleSymbolsReduce();
					
					for(int sym : symbol){
						if(!((state.stackSize() > 2 || !state.isQueueEmpty()) && (state.isSymbolGoal(sym) || state.isSymbolSentence(sym)))){
							State newState = new State(state);
							
							newState.reduce(sym, true);
							newState.setDerivation(state);
							newState.setStack(state.stack().stack());
							newState.setReduced(state.stack());
							newState.setSpan(new Span(state.stack().span().first(), state.span().last()));

							temp.add(newState);
						}
					}
				}else if(entry == Operation.UNARY){
					List<Integer> symbol = state.possibleSymbolsUnary();

					for(int sym : symbol){
						if(!((state.stackSize() > 1 || !state.isQueueEmpty()) && (state.isSymbolGoal(sym) || state.isSymbolSentence(sym)))){
							State newState = new State(state);

							newState.unary(sym, true);
							newState.setDerivation(state);
							newState.setStack(state.stack());
							newState.setReduced(null);
							newState.setSpan(state.span());
							
							temp.add(newState);
						}
					}
				}else if(entry == Operation.FINAL){
					State newState = new State(state);
					
					newState.finish(true);
					newState.setDerivation(state);
					newState.setStack(state.stack());
					newState.setReduced(null);
					newState.setSpan(state.span());
					
					temp.add(newState);
				}else if(entry == Operation.IDLE){					
					State newState = new State(state);

					newState.idle(true);
					newState.setDerivation(state);
					newState.setStack(state.stack());
					newState.setReduced(null);
					newState.setSpan(state.span());
					
					temp.add(newState);
				}				
			}
        }
		
		currentAgenda = temp;
		currentAgenda = prune(currentAgenda, beamSize);
		currentAgenda = Lists.reverse(currentAgenda);
	}
	
	/**
	 * Expand next possible alternative state from current agenda if last agenda is empty
	 * @throws Exception
	 */
	public void nextAgendaAlternate() throws Exception{
		final List<State> temp = new ArrayList<State>();

		for(final State state : currentAgenda){
			final List<Integer> actions = state.possibleAlternateActions();
		
			for(int entry : actions){
				if(entry == Operation.SHIFT){
					List<Integer> symbol = state.possibleSymbolsShift();
					
					for(int sym : symbol){
						State newState = new State(state);
						
						newState.shift(sym, true);
						newState.setDerivation(state);
						newState.setStack(state);
						newState.setReduced(null);
						newState.setSpan(new Span(state.next(), state.next() + 1));
						
						temp.add(newState);
					}
				}else if(entry == Operation.REDUCE){
					State newState = new State(state);
					if(state.stackSize() > 2){
						newState.reduce(state.grammar().sentenceBinarized(), true);
						newState.setDerivation(state);
						newState.setStack(state.stack().stack());
						newState.setReduced(state.stack());
						newState.setSpan(new Span(state.stack().span().first(), state.span().last()));
						
						temp.add(newState);
					}else{
						if(state.isQueueEmpty()){
							newState.reduce(state.grammar().sentence(), true);
							newState.setDerivation(state);
							newState.setStack(state.stack().stack());
							newState.setReduced(state.stack());
							newState.setSpan(new Span(state.stack().span().first(), state.span().last()));
							
							temp.add(newState);
						}
					}
				}else if(entry == Operation.UNARY){
					if(state.isQueueEmpty() && !state.isSymbolGoal(state.currentHead())){
						State newState = new State(state);

						newState.unary(state.grammar().goal(), true);
						newState.setDerivation(state);
						newState.setStack(state.stack());
						newState.setReduced(null);
						newState.setSpan(state.span());
						
						temp.add(newState);
					}
				}else if(entry == Operation.FINAL){
					State newState = new State(state);
					
					newState.finish(true);
					newState.setDerivation(state);
					newState.setStack(state.stack());
					newState.setReduced(null);
					newState.setSpan(state.span());
					
					temp.add(newState);
				}else if(entry == Operation.IDLE){					
					State newState = new State(state);
					
					newState.idle(true);
					newState.setDerivation(state);
					newState.setStack(state.stack());
					newState.setReduced(null);
					newState.setSpan(state.span());
					
					temp.add(newState);
				}				
			}				
        }
		
		currentAgenda = temp;
	}
		
	/**
	 * Beam search
	 * @param candidates
	 * @throws Exception
	 */
	public void parse(List<List<State>> candidates) throws Exception{
		candidates.clear();
		
		int stepLast = (2 + initialState.maxConsUnary()) * initialState.queueSize();
		
		for(int step = 0; step <= stepLast; step++){
			//System.out.println(currentAgenda().size() + " " + currentAgenda());
			
			if(step > 0){
				agenda.add(currentAgenda);
			}
			nextAgenda();
		}

		if(agenda.get(stepLast).isEmpty()){
			int stepBack = stepLast - 1;
			
			while(stepBack >= 0 && agenda.get(stepBack).isEmpty()){
				stepBack--;
			}
			
			currentAgenda = agenda.get(stepBack);
						
			for(int step = stepBack; step <= stepLast; step++){
				if(step > stepBack){
					currentAgenda = prune(currentAgenda, beamSize);
					currentAgenda = Lists.reverse(currentAgenda);
				}
				
				//System.out.println(currentAgenda().size() + " " + currentAgenda());
				agenda.set(step, currentAgenda);
				nextAgendaAlternate();
			}
		}
		
		// prune last agenda to kbest
		currentAgenda = prune(agenda.get(stepLast), kbest);
		List<State> unreversedAgenda = currentAgenda;
		currentAgenda = Lists.reverse(currentAgenda);
		agenda.set(stepLast, currentAgenda);
		
		candidates.add(unreversedAgenda);
	}
	
	/**
	 * Beam search
	 * @throws Exception
	 */
	public void parse() throws Exception{
		int stepLast = (2 + initialState.maxConsUnary()) * initialState.queueSize();
		
		for(int step = 0; step <= stepLast; step++){
			//System.out.println(currentAgenda().size() + " " + currentAgenda());
			
			if(step > 0){
				agenda.add(currentAgenda);
			}
			nextAgenda();
		}

		if(agenda.get(stepLast).isEmpty()){
			int stepBack = stepLast - 1;
			
			while(stepBack >= 0 && agenda.get(stepBack).isEmpty()){
				stepBack--;
			}
			
			currentAgenda = agenda.get(stepBack);
						
			for(int step = stepBack; step <= stepLast; step++){
				if(step > stepBack){
					currentAgenda = prune(currentAgenda, beamSize);
					currentAgenda = Lists.reverse(currentAgenda);
				}
				
				//System.out.println(currentAgenda().size() + " " + currentAgenda());
				agenda.set(step, currentAgenda);
				nextAgendaAlternate();
			}
		}
		
		// prune last agenda to kbest
		currentAgenda = prune(agenda.get(stepLast), kbest);
		currentAgenda = Lists.reverse(currentAgenda);
		agenda.set(stepLast, currentAgenda);
	}
	
	/**
	 * Order list of states in ascending order with size limitation
	 * @param iterable
	 * @param size
	 * @return
	 */
	public List<State> prune(List<State> iterable, int size){				
		if(iterable.size() < size){
			return com.google.common.collect.Ordering.natural().greatestOf(iterable, iterable.size());
		}else{
			return com.google.common.collect.Ordering.natural().greatestOf(iterable, size);
		}
	}
	
	/**
	 * Get current agenda
	 * @return
	 */
	public List<State> currentAgenda(){
		return currentAgenda;
	}
	
	/**
	 * Get all agenda from initial to final step
	 * @return
	 */
	public List<List<State>> agenda(){
		return agenda;
	}
	
	/**
	 * Get hidden dimension size of state
	 * @return
	 */
	public int hidden(){
		return initialState.hidden();
	}
	
	/**
	 * Get input sentence size
	 * @return
	 */
	public int inputSize(){
		return initialState.queueSize();
	}
	
	/**
	 * Get input queue sentence
	 * @return
	 */
	public Queue queue(){
		return initialState.queue();
	}
	
	/**
	 * Get input queue sentence represented in array of String
	 * @return
	 */
	public String[] sentence(){
		return initialState.queue().words();
	}
}
