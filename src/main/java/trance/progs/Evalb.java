package trance.progs;

public class Evalb {
	private int match;
	private int gold;
	private int test;
	
	/**
	 * Default constructor
	 */
	public Evalb(){
		this.match = 0;
		this.gold = 0;
		this.test = 0;
	}
	
	/**
	 * User-defined constructor
	 * @param match
	 * @param gold
	 * @param test
	 */
	public Evalb(int match, int gold, int test){
		this.match = match;
		this.gold = gold;
		this.test = test;
	}
	
	/**
	 * Reset all values to 0
	 */
	public void clear(){
		this.match = 0;
		this.gold = 0;
		this.test = 0;
	}
	
	/**
	 * Add Evalb in place
	 * @param other value of Evalb to be added in place
	 * @return
	 */
	public Evalb addi(Evalb other){
		this.match += other.match;
		this.gold += other.gold;
		this.test += other.test;
		
		return this;
	}
	
	/**
	 * Substract Evalb in place 
	 * @param other value of Evalb to be substracted in place
	 * @return
	 */
	public Evalb subi(Evalb other){
		this.match -= other.match;
		this.gold -= other.gold;
		this.test -= other.test;
		
		return this;
	}
	
	/**
	 * Compute recall
	 * @return
	 */
	public double recall(){
		if(this.gold == 0){
			return 0.0;
		}else{
			return (double) match / (double) gold;
		}
	}
	
	/**
	 * Compute precision
	 * @return
	 */
	public double precision(){
		if(this.test == 0){
			return 0.0;
		}else{
			return (double) match / (double) test;
		}
	}
	
	/**
	 * Compute balanced F1 measure
	 * @return
	 */
	public double f(){
		if(this.match == 0 || this.gold == 0 || this.test == 0){
			return 0.0;
		}else{
			double p = precision();
			double r = recall();
			
			return 2 * p * r / (p + r);
		}
	}
	
	/**
	 * Execute Evalb
	 * @return
	 */
	public double execute(){
		return f();		
	}
}
