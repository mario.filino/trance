package trance.progs;

import java.util.ArrayList;
import java.util.List;

import trance.exceptions.HiddenVectorSizeException;

public class OracleParser {
	private State initialState;
	private List<State> currentAgenda;
	private List<List<State>> agenda;
	private Oracle oracle;
	
	public OracleParser(State state, Oracle oracle){
		this.oracle = oracle;
		initialState = new State(state);

		currentAgenda = new ArrayList<State>();
		agenda = new ArrayList<List<State>>();
		
		initAgenda();
	}
	
	private void initAgenda(){
		currentAgenda.add(initialState);
		agenda.add(currentAgenda);
	}
	
	public void parse() throws Exception{
		int stepLast = (2 + initialState.maxConsUnary()) * initialState.queueSize();
		int agendaSize = stepLast + 1;
		
		if(oracle.actions().size() >= agendaSize)
			throw new RuntimeException("oracle sequences of actions is longer than agenda size");
				
		for(int step = 0; step <= stepLast; step++){
			//System.out.println(currentAgenda().size() + " " + currentAgenda());
			
			List<State> temp = new ArrayList<State>();
			
			if(step > 0){
				agenda.add(currentAgenda);
			}
			
			for(State state : currentAgenda){
				State tempState = new State(state);
				
				if(state.isParsingFinish()){					
					tempState.idle(true);
					tempState.setDerivation(state);
					tempState.setStack(state.stack());
					tempState.setReduced(null);
					
					temp.add(tempState);
				}else{
					if(step < oracle.actions().size()){
						switch(oracle.actions().get(step).action()){
							case Operation.SHIFT:
								if(state.isQueueEmpty())
									throw new RuntimeException("invalid shift");
								
								tempState.shift(oracle.actions().get(step).symbol(), true);
								tempState.setDerivation(state);
								tempState.setStack(state);
								tempState.setReduced(null);
								
								temp.add(tempState);
								
								break;
							case Operation.REDUCE:
								if(state.stackSize() < 2)
									throw new RuntimeException("invalid reduce");
								
								tempState.reduce(oracle.actions().get(step).symbol(), true);
								tempState.setDerivation(state);
								tempState.setStack(state.stack().stack());
								tempState.setReduced(state.stack());
								
								temp.add(tempState);
								
								break;
							case Operation.UNARY:
								if(state.stackSize() < 1 || state.currentConsUnary() > state.maxConsUnary() || state.currentUnaryCount() > state.maxUnary())
									throw new RuntimeException("invalid unary");	
								
								tempState.unary(oracle.actions().get(step).symbol(), true);
								tempState.setDerivation(state);
								tempState.setStack(state.stack());
								tempState.setReduced(null);
								
								temp.add(tempState);
								
								break;
							default:
								throw new RuntimeException("invalid operation");
						}
					}else{
						if(state.isQueueEmpty() && state.stackSize() < 2 && state.isRootReached()){
							tempState.finish(true);
							tempState.setDerivation(state);
							tempState.setStack(state.stack());
							tempState.setReduced(null);
							
							temp.add(tempState);
						}else{
							throw new RuntimeException("invalid final");
						}
					}
				}
			}
			
			currentAgenda = temp;
		}
		
		currentAgenda = agenda.get(stepLast);
	}
	
	public List<List<State>> agenda(){
		return agenda;
	}
	
	public List<State> currentAgenda(){
		return currentAgenda;
	}
}
