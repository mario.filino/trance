package trance.progs;

import org.jblas.FloatMatrix;

import trance.exceptions.HiddenVectorSizeException;

/**
 * @author Mario Filino
 *
 */
public class QueueElement {
    private FloatMatrix hiddenState;
    private FloatMatrix wordVector;

    /**
     * Constructor
     * 
     * @param hiddenDimSize
     * @param wordEmbedDimSize
     */
    public QueueElement(int hiddenDimSize, int wordEmbedDimSize){       
        hiddenState = new FloatMatrix(hiddenDimSize, 1);
        wordVector = new FloatMatrix(wordEmbedDimSize, 1);
    }
    
    /**
     * Copy constructor
     * 
     * @param n
     */
    public QueueElement(QueueElement n){
    	this.hiddenState = n.hiddenState.getColumn(0);
    	this.wordVector = n.wordVector.getColumn(0);
    }

    public void setHiddenState(FloatMatrix hidden) throws HiddenVectorSizeException{
    	if(hidden.rows != hiddenState.rows || hidden.columns != hiddenState.columns)
    		throw new HiddenVectorSizeException("Vector size doesn't match");
    	
        hiddenState = hidden;
    }

    public void setWordVector(FloatMatrix word) throws HiddenVectorSizeException{
    	if(word.rows != wordVector.rows || word.columns != wordVector.columns)
    		throw new HiddenVectorSizeException("Vector size doesn't match");

        wordVector = word;
    }
    
    public FloatMatrix getHiddenState(){
    	return hiddenState;
    }
    
    public FloatMatrix getWordVector(){
    	return wordVector;
    }
}
