package trance.progs;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import edu.stanford.nlp.trees.PennTreeReader;
import edu.stanford.nlp.trees.TreeReader;

public class Tree {
	private List<edu.stanford.nlp.trees.Tree> trees;
	private List<String[]> sentences;
	
	public Tree(){
		trees = new ArrayList<edu.stanford.nlp.trees.Tree>();
		sentences = new ArrayList<String[]>();
	}
	
	public void loadTree(String path) throws IOException{
		File modelFile = new File(path);
    	
    	BufferedReader reader = new BufferedReader(new FileReader(modelFile));
    	TreeReader tr = new PennTreeReader(reader);
    	
    	edu.stanford.nlp.trees.Tree tree;
    	
    	while((tree = tr.readTree()) != null){
    		trees.add(tree);
	    	List<edu.stanford.nlp.trees.Tree> sentence = tree.getLeaves();
	    	
	    	String[] words = new String[sentence.size()];
	    	for(int i = 0; i < sentence.size(); i++){
	    		words[i] = sentence.get(i).value();
	    	}
	    	
	    	sentences.add(words);
    	}
    	
    	tr.close();
    	reader.close();
	}
	
	public List<edu.stanford.nlp.trees.Tree> tree(){
		return trees;
	}
	
	public List<String[]> sentences(){
		return sentences;
	}
	
	public int size(){
		return sentences.size();
	}
}
