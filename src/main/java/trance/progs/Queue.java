package trance.progs;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

import org.jblas.FloatMatrix;

import edu.stanford.nlp.ling.HasWord;
import edu.stanford.nlp.process.DocumentPreprocessor;
import trance.exceptions.HiddenVectorSizeException;
import trance.exceptions.WordNotFoundException;
import trance.exceptions.WordVectorSizeException;
import trance.model.Model;

/**
 * @author Mario Filino
 *
 */
public class Queue {
	private int hiddenDimSize;
    private int wordEmbedDimSize;
    private String[] words;
    private List<QueueElement> queue;
    private FloatMatrix initialHiddenState;
    private int currentIndex;
    private Model param;
    private Terminal vec;

     public Queue(String sentence, boolean lowercase, Model param, Terminal vec) throws Exception{
        hiddenDimSize = param.hiddenSize();
        wordEmbedDimSize = param.embeddingSize();
        
        currentIndex = 0;

        words = sentenceToWords(sentence, lowercase);
        queue = new ArrayList<QueueElement>(words.length + 1);

        initialHiddenState = new FloatMatrix(hiddenDimSize, 1);

        for(int i = 0; i <= words.length; i++){
            QueueElement elem = new QueueElement(hiddenDimSize, wordEmbedDimSize);
            queue.add(elem);
        }
        
        this.param = param;
        initHiddenState(this.param.getBqe());
        
        QueueElement initialState = new QueueElement(hiddenDimSize, wordEmbedDimSize);
        initialState.setHiddenState(this.param.getBqe());
        queue.add(initialState);
        
        this.vec = vec;
        createWordsVector();
    }
     
     public Queue(String[] sentence, boolean lowercase, Model param, Terminal vec) throws Exception{
         hiddenDimSize = param.hiddenSize();
         wordEmbedDimSize = param.embeddingSize();
         
         currentIndex = 0;

         words = sentence;
         queue = new ArrayList<QueueElement>(words.length + 1);

         initialHiddenState = new FloatMatrix(hiddenDimSize, 1);

         for(int i = 0; i <= words.length; i++){
             QueueElement elem = new QueueElement(hiddenDimSize, wordEmbedDimSize);
             queue.add(elem);
         }
         
         this.param = param;
         initHiddenState(this.param.getBqe());
         
         this.vec = vec;
         createWordsVector();
     }

    /**
     * Copy constructor
     * 
     * @param n
     */
    public Queue(Queue n){
    	this.hiddenDimSize = n.hiddenDimSize;
    	this.wordEmbedDimSize = n.wordEmbedDimSize;
    	this.currentIndex = n.currentIndex;
    	this.initialHiddenState = n.initialHiddenState;		
    	this.words = n.words;
    	this.queue = n.queue;
    	this.param = n.param;
    	this.vec = n.vec;
    }
    
    @Override
    public String toString(){
    	String output;
		
		if(isEmpty()){
			output = "[]";
		}else{
			output = "[ ";
			for(int i = currentIndex; i < words.length; i++){
				output += words[i];
				output += " ";
			}
			output += "]";
		}
		
		return output;
    }

    private String[] sentenceToWords(String input, boolean lowercase) throws IOException{
    	String[] token = null;
		
    	Reader reader = new StringReader(input);
		DocumentPreprocessor dp = new DocumentPreprocessor(reader);
		
		for(List<HasWord> sentence : dp){
			token = new String[sentence.size()];
			for(int i = 0; i < sentence.size(); i++){
				if(lowercase){
					token[i] = sentence.get(i).word().toLowerCase();
				}else{
					token[i] = sentence.get(i).word();
				}
			}
		}

        return token;
    }
    
    public void initHiddenState(FloatMatrix hiddenState) throws HiddenVectorSizeException{
        if(hiddenState.rows != hiddenDimSize || hiddenState.columns > 1)
            throw new HiddenVectorSizeException("Hidden vector size doesn't match initialized hidden dimension size");
        
        initialHiddenState = hiddenState;
        queue.get(words.length).setHiddenState(Model.activation(initialHiddenState));
    }
    
    public void initHiddenState(String path) throws IOException, HiddenVectorSizeException{
    	File hiddenStateFile = new File(path);
    	
    	BufferedReader reader = new BufferedReader(new FileReader(hiddenStateFile));
    	String line = reader.readLine();
    	String[] initial = line.split(" ");
    	
    	int hiddenStateSize = Integer.parseInt(initial[0]);
    	
    	float[] buffer = new float[hiddenStateSize]; 	
    	int i = 0;
    	
    	while((line = reader.readLine()) != null){	
    		if(line.isEmpty()){
    			continue;
    		}
    		
    		float value = Float.parseFloat(line);
    		buffer[i] = value;
    		i++;
    	}
    	
    	reader.close();
    	
    	FloatMatrix hiddenState = new FloatMatrix(buffer);
    	initHiddenState(hiddenState);
    }
    
    public static FloatMatrix readHiddenState(String path) throws IOException, HiddenVectorSizeException{
    	File hiddenStateFile = new File(path);
    	
    	BufferedReader reader = new BufferedReader(new FileReader(hiddenStateFile));
    	String line = reader.readLine();
    	String[] initial = line.split(" ");
    	
    	int hiddenStateSize = Integer.parseInt(initial[0]);
    	
    	float[] buffer = new float[hiddenStateSize]; 	
    	int i = 0;
    	
    	while((line = reader.readLine()) != null){	
    		if(line.isEmpty()){
    			continue;
    		}
    		
    		float value = Float.parseFloat(line);
    		buffer[i] = value;
    		i++;
    	}
    	
    	reader.close();
    	
    	FloatMatrix hiddenState = new FloatMatrix(buffer);
    	
    	return hiddenState;
    }

    public void createWordsVector() throws WordVectorSizeException, WordNotFoundException, HiddenVectorSizeException{
    	for(int i = 0; i < words.length; i++){
    		FloatMatrix temp = vec.terminal(words[i]);
    		if(temp.rows != wordEmbedDimSize)
        		throw new WordVectorSizeException("Inputted embedding size doesn't match embedding size of word vector");
    		
    		queue.get(i).setWordVector(temp);
    	}
    }

    public void computeHiddenState() throws HiddenVectorSizeException, Exception{
    	for(int i = words.length - 1; i >= 0; i--){
    		FloatMatrix temp = new FloatMatrix(hiddenDimSize, 1);
    		
    		temp.addi(param.getHqu().mmul(queue.get(i+1).getHiddenState()));
    		temp.addi(param.getWqu().mmul(queue.get(i).getWordVector()));
 
    		temp.addi(param.getBqu());
    		
    		queue.get(i).setHiddenState(Model.activation(temp));
    	}
    }
    
    public QueueElement peek(){
    	return queue.get(currentIndex);
    }
    
    public QueueElement remove(){
    	if(currentIndex < words.length){
    		QueueElement elem = queue.get(currentIndex);
    		currentIndex++;
    		
    		return elem;
    	}else{
    		throw new ArrayIndexOutOfBoundsException();
    	}
    }
    
    public String currentWord(){
    	if(currentIndex < words.length){
    		return words[currentIndex];
    	}else{
    		throw new ArrayIndexOutOfBoundsException();
    	}
    }
    
    public String[] words(){
    	return words;
    }
    
    public int size(){
    	return words.length;
    }
    
    public int currentSize(){
    	return words.length - currentIndex;
    }
    
    public boolean isEmpty(){
    	return words.length - currentIndex == 0;
    }
    
    public FloatMatrix initialHiddenState(){
    	return initialHiddenState;
    }
    
    public int currentIndex(){
    	return currentIndex;
    }
    
    public FloatMatrix col(int col){
    	return queue.get(col).getHiddenState();
    }
}
