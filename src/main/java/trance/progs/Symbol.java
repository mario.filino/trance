package trance.progs;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Map;

public class Symbol {
	private String[] nonTerminal;
	
	private static final String AXIOM = "[-AXIOM-]";
	private static final String FINAL = "[-FINAL-]";
	private static final String IDLE = "[-IDLE-]";
	
	private static final String UNK = "<unk>";
	private static final String EPSILON = "<epsilon>";

	private Map<Integer, String> symbol;
	private Map<String, Integer> index;
	
	private String path;
	
	public Symbol(String path){
		this.path = path;
		loadNonTerminalSymbols(this.path);
		map();
	}
	
	private void map(){
		symbol = new HashMap<Integer, String>();
		index = new HashMap<String, Integer>();
		
		for(int i = 0; i < nonTerminal.length; i++){
			index.put(nonTerminal[i], i);
			symbol.put(i, nonTerminal[i]);
		}
	}
	
	private void loadNonTerminalSymbols(String path){		
		File file = new File(path);
		
		ArrayList<String> symbols = new ArrayList<String>();
		
		symbols.add(AXIOM);
		symbols.add(FINAL);
		symbols.add(IDLE);
		
		try {
			BufferedReader reader = new BufferedReader(new FileReader(file));
			String line;
			
			while((line = reader.readLine()) != null){
				if(!line.isEmpty()){
					String[] grammar = line.split(" ");
					symbols.add(grammar[0]);
				}
			}
			
			reader.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		LinkedHashSet<String> set = new LinkedHashSet<String>();
		set.addAll(symbols);
		symbols.clear();
		symbols.addAll(set);
		
		nonTerminal = new String[symbols.size()];
		nonTerminal = symbols.toArray(nonTerminal);		
	}
	
	public String path(){
		return path;
	}
	
	public String[] nonTerminal(){
		return nonTerminal;
	}
	
	public Map<String, Integer> index(){
		return index;
	}
	
	public Map<Integer, String> symbol(){
		return symbol;
	}
	
	public static String signature(String word){
		return UNK;
	}
	
	public static String unknown_(){
		return UNK;
	}
	
	public static String epsilon_(){
		return EPSILON;
	}
	
	public int axiom_(){
		return index.get(AXIOM);
	}
	
	public int final_(){
		return index.get(FINAL);
	}
	
	public int idle_(){
		return index.get(IDLE);
	}
	
	public int size(){
		return index.size();
	}
	
	public boolean binarized(int label){
		if(symbol.get(label) == null)
			return false;
		
		return symbol.get(label).contains("^");
	}
}
