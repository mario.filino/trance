package trance.progs;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;

import org.jblas.FloatMatrix;

/**
 * @author Mario Filino
 *
 */
public class Terminal {
	private int vocabSize;
	private int embeddingSize;
	private HashMap<String, float[]> wordVectors;

    public Terminal(String path) throws IOException {
        wordVectors = new HashMap<String, float[]>();
                
        loadTextModel(path);
    }
    
    private void loadTextModel(String path) throws IOException{
    	File modelFile = new File(path);
    	
    	BufferedReader reader = new BufferedReader(new FileReader(modelFile));
    	String line = reader.readLine();
    	String[] initial = line.split(" ");
    	
    	vocabSize = Integer.parseInt(initial[0]);
    	embeddingSize = Integer.parseInt(initial[1]);
    	
    	while((line = reader.readLine()) != null){
    		String[] split = line.split(" ");
    		String word = split[0];
    		
    		if(word.isEmpty()){
    			continue;
    		}
    		
    		float[] buffer = new float[embeddingSize];
    		for(int i = 1; i < split.length; i++){
    			float parsedFloat = Float.parseFloat(split[i]);    			
    			buffer[i - 1] = parsedFloat;
    		}
    		
    		wordVectors.put(word, buffer);
    	}
    	
    	reader.close();
    }
    
    public int getVocabSize(){
    	return this.vocabSize;
    }
    
    public int getEmbeddingSize(){
    	return this.embeddingSize;
    }
    
    public boolean isTerminal(String word){
    	return wordVectors.get(word) != null;
    }
    
    public FloatMatrix terminal(String word){
    	float[] floatVec = wordVectors.get(word);
    	
    	if(floatVec == null){
    		floatVec = wordVectors.get(Symbol.signature(word));
    	}
    	
    	return new FloatMatrix(embeddingSize, 1, floatVec);
    }
}
