package trance.progs;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import edu.stanford.nlp.trees.LabeledScoredTreeFactory;
import edu.stanford.nlp.trees.Tree;
import edu.stanford.nlp.trees.TreeFactory;

public class Oracle {
	private List<String> sentence;
	private Tree binarized;
	private List<TransitionAction> actions;
	
	private Symbol sym;
	
	public Oracle(Symbol symbol){
		sentence = new ArrayList<String>();
		actions = new ArrayList<TransitionAction>();
		
		TreeFactory treeFactory = new LabeledScoredTreeFactory();
		binarized = treeFactory.newLeaf("temp");
		
		sym = symbol;
	}
	
	public List<TransitionAction> actions(){
		return actions;
	}
	
	public String[] sentence(){
		return sentence.stream().toArray(String[]::new);
	}
	
	public Tree tree(){
		return binarized;
	}
	
	public void assign(Tree tree, boolean left){
		TreeFactory treeFactory = new LabeledScoredTreeFactory();
		
		if(left){
			BinarizeLeft bl = new BinarizeLeft(treeFactory);
			bl.binarize(tree, binarized);
		}else{
			BinarizeRight br = new BinarizeRight(treeFactory);
			br.binarize(tree, binarized);
		}
		
		oracle(binarized);
	}
	
	private void oracle(Tree tree){
		// post-traversal to compute SR sequence
		switch(tree.children().length){
			case 1:
				if(tree.children()[0].children().length == 0){
					sentence.add(tree.children()[0].label().value());
					actions.add(new TransitionAction(Operation.SHIFT, sym.index().get("[" + tree.label().value() + "]")));
				}else{
					oracle(tree.children()[0]);
					actions.add(new TransitionAction(Operation.UNARY, sym.index().get("[" + tree.label().value() + "]")));
				}
				break;
				
			case 2:
				oracle(tree.children()[0]);
				oracle(tree.children()[1]);
				actions.add(new TransitionAction(Operation.REDUCE, sym.index().get("[" + tree.label().value() + "]")));			
				break;
				
			default: 
				throw new RuntimeException("invalid tree structure");
		}
	}
	
	public static void main(String[] args) throws IOException{
		Symbol symbol = new Symbol("example/grammar.txt");
				
		trance.progs.Tree trees = new trance.progs.Tree();
		trees.loadTree("example/WSJ-train.treebank");
		System.out.println(trees.tree().get(1).pennString());

		Oracle oracle = new Oracle(symbol);
		oracle.assign(trees.tree().get(0), false);
		
		System.out.println(Arrays.asList(oracle.sentence()));
	}
}
