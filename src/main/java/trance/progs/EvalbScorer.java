package trance.progs;

import java.util.ArrayList;
import java.util.List;

import org.apache.xalan.xsltc.compiler.sym;
import org.javatuples.Pair;

import edu.stanford.nlp.trees.LabeledScoredTreeFactory;

import edu.stanford.nlp.trees.Tree;
import edu.stanford.nlp.trees.TreeFactory;

public class EvalbScorer {
	private List<Pair<Span, Integer>> gold;
	private List<Pair<Span, Integer>> test;
	
	private Symbol symbol;
	
	/**
	 * Default constructor
	 */
	public EvalbScorer(Symbol sym){
		this.gold = new ArrayList<>();
		this.test = new ArrayList<>();
		this.symbol = sym;
	}
	
	/**
	 * User-defined constructor using state
	 * @param state
	 */
	public EvalbScorer(State state, Symbol sym){
		this.gold = new ArrayList<>();
		this.test = new ArrayList<>();
		this.symbol = sym;
		assign(state);
	}
	
	/**
	 * User-defined constructor using tree
	 * @param tree
	 */
	public EvalbScorer(Tree tree, Symbol sym){
		this.gold = new ArrayList<>();
		this.test = new ArrayList<>();
		this.symbol = sym;
		assign(tree);
	}
	
	/**
	 * Assign state to gold Span Symbol pair
	 * @param state
	 */
	public void assign(State state){
		collect(state, gold);
	}
	
	/**
	 * Assign tree to gold Span Symbol pair
	 * @param tree
	 */
	public void assign(Tree tree){
		collect(tree, gold);
	}
	
	/**
	 * Create Evalb
	 * @param state
	 * @return
	 */
	public Evalb execute(State state){
		collect(state, test);
		
		return new Evalb(matched(), gold.size(), test.size());
	}
	
	/**
	 * Create Evalb
	 * @param tree
	 * @return 
	 */
	public Evalb execute(Tree tree){
		collect(tree, test);
		
		return new Evalb(matched(), gold.size(), test.size());
	}
	
	/**
	 * Compute less than of two Spans
	 * @param x
	 * @param y
	 * @return
	 */
	private boolean spanLessThan(Span x, Span y){
		return x.first() < y.first() || (! (y.first() < x.first()) && x.last() < y.last());
	}
	
	/**
	 * Compute less than of two pairs
	 * @param lhs
	 * @param rhs
	 * @return
	 */
	private boolean pairLessThan(Pair<Span, Integer> lhs, Pair<Span, Integer> rhs){
		if(spanLessThan(lhs.getValue0(), rhs.getValue0()))
			return true;
		else if(spanLessThan(rhs.getValue0(), lhs.getValue0()))
			return false;
		else if(lhs.getValue1() < rhs.getValue1())
			return true;
		else
			return false;
	}
	
	/**
	 * Compute match
	 * @return
	 */
	private int matched(){
		int match = 0;
		
		int goldIterator = 0;
		int goldSize = gold.size();
		
		int testIterator = 0;
		int testSize = test.size();
		
		while(goldIterator != goldSize && testIterator != testSize){
			if(pairLessThan(gold.get(goldIterator), test.get(testIterator))){
				goldIterator++;
			}else if(pairLessThan(test.get(testIterator), gold.get(goldIterator))){
				testIterator++;
			}else{
				match++;
				goldIterator++;
				testIterator++;
			}
		}
		
		return match;
	}
	
	private void collect(State state, List<Pair<Span, Integer>> stats){
		stats.clear();
		
		while(state != null){
			switch(state.operation()){
				case Operation.REDUCE:
				case Operation.UNARY:
					if(!symbol.binarized(state.label())){
						stats.add(new Pair<Span, Integer>(state.span(), state.label()));
					}
					break;
				default:
					break;
			}
			
			state = state.derivation();
		}
		
		// sort
	}
	
	private void collect(Tree tree, List<Pair<Span, Integer>> stats){
		stats.clear();
		
		Span span = new Span(0, 0);
		this.collect(tree, span, stats);
		
		// sort
	}
	
	private void collect(Tree tree, Span span, List<Pair<Span, Integer>> stats){
		boolean pos = false;
	}
}
