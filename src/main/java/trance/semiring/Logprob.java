package trance.semiring;

import org.apache.commons.logging.Log;

public class Logprob {
	private double value;
	
	public Logprob(){
		value = Double.NEGATIVE_INFINITY;
	}
	
	public Logprob(double x){
		value = x;
	}
	
	public Logprob plus(double x){
		if(value == Double.NEGATIVE_INFINITY){
			value = x;
			return this;
		}else if(x == Double.NEGATIVE_INFINITY){
			return this;
		}
		
		if(value <= x){
			value = x + Math.log1p(Math.exp(value - x));
		}else{
			value = value + Math.log1p(Math.exp(x - value));
		}
		
		return this;
	}
	
	public Logprob minus(double x){
		if(x > value){
			throw new RuntimeException("invalid minus");
		}
		
		if(value == x){
			value = Double.NEGATIVE_INFINITY;
		}else{
			double expValue = Math.exp(x - value);
			
			if(Double.compare(expValue, 1.0) == 0){
				value = Double.NEGATIVE_INFINITY;
			}else{
				value = value + Math.log1p(-expValue);
			}
		}
		
		return this;
	}
	
	public Logprob mul(double x){
		value += x;
		return this;
	}
	
	public Logprob div(double x){
		value -= x;
		return this;
	}
	
	public double value(){
		return value;
	}
	
	public String toString(){
		return value + "";
	}
}
