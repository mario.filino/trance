package trance.optimize;

import org.jblas.FloatMatrix;

import trance.model.Model;

public class Adadec {
	private Model G;
	
	private double lambda;
	private double eta0;
	private double epsilon;
	private double gamma;
	
	public Adadec(Model G, double lambda, double eta0, double epsilon, double gamma){
		this.G = new Model(G);
		this.G.setZero();
		this.lambda = lambda;
		this.eta0 = eta0;
		this.epsilon = epsilon;
		this.gamma = gamma;
	}
	
	public double learningRate(double eta0, double epsilon, double g){
		return eta0 / Math.sqrt(Math.max(1e-40, epsilon + g));
	}
	
	public double decay(){
		eta0 *= 0.5;
		return eta0;
	}
	
	private void update(FloatMatrix[] param, FloatMatrix[] G, FloatMatrix[] grad, double scale, boolean regularize){
		if(regularize && Double.compare(lambda, 0.0) != 0){
			for(int i = 0; i < param.length; i++){
				for(int index = 0; index < param[i].length; index++){
					double g = (double) grad[i].get(index);
					
					if(Double.compare(g, 0.0) != 0){
						double newG = (double) G[i].get(index) * gamma + (g * scale) * (g * scale);
						G[i].put(index, (float) newG);
						
						double rate = learningRate(eta0, epsilon, newG);
						double x1 = param[i].get(index) - rate * scale * g;
						
						param[i].put(index, (float) (Math.signum(x1) * Math.max(0.0, Math.abs(x1) - rate * lambda)));
					}
				}
			}
		}else{
			for(int i = 0; i < param.length; i++){
				for(int index = 0; index < param[i].length; index++){
					double g = grad[i].get(index);
					
					if(Double.compare(g, 0.0) != 0){
						double newG = (double) G[i].get(index) * gamma + (g * scale) * (g * scale);
						G[i].put(index, (float) newG);
						
						double rate = learningRate(eta0, epsilon, newG);
						
						param[i].put(index, (float) ((double) param[i].get(index) - rate * scale * g));
					}
				}
			}
		}
	}
		
	private void update(FloatMatrix param, FloatMatrix G, FloatMatrix grad, double scale, boolean regularize){
		if(regularize && Double.compare(lambda, 0.0) != 0){
			for(int index = 0; index < param.length; index++){
				double g = grad.get(index);
				
				if(Double.compare(g, 0.0) != 0){
					double newG = (double) G.get(index) * gamma * (g * scale) * (g * scale);
					G.put(index, (float) newG);
					
					double rate = learningRate(eta0, epsilon, newG);
					double x1 = param.get(index) - rate * scale * g;
					
					param.put(index, (float) (Math.signum(x1) * Math.max(0.0, Math.abs(x1) - rate * lambda)));
				}
			}
		}else{
			for(int index = 0; index < param.length; index++){
				double g = grad.get(index);
				
				if(Double.compare(g, 0.0) != 0){
					double newG = (double) G.get(index) * gamma * (g * scale) * (g * scale);
					G.put(index, (float) newG);
					
					double rate = learningRate(eta0, epsilon, newG);
					
					param.put(index, (float) ((double) param.get(index) - rate * scale * g));
				}
			}
		}
	}
	
	public void update(Model param, trance.gradient.Model gradient) throws InterruptedException{
		if(gradient.count == 0){
			return;
		}
		
		double scale = 1.0 / gradient.count;
				
		update(param.getHsh(), G.getHsh(), gradient.getHsh(), scale, true);
		update(param.getQsh(), G.getQsh(), gradient.getQsh(), scale, true);
		update(param.getWsh(), G.getWsh(), gradient.getWsh(), scale, true);
		update(param.getBsh(), G.getBsh(), gradient.getBsh(), scale, false);
		update(param.getVsh(), G.getVsh(), gradient.getVsh(), scale, true);
		update(param.getvsh(), G.getvsh(), gradient.getvsh(), scale, false);
		
		update(param.getHre(), G.getHre(), gradient.getHre(), scale, true);
		update(param.getQre(), G.getQre(), gradient.getQre(), scale, true);
		update(param.getWre0(), G.getWre0(), gradient.getWre0(), scale, true);
		update(param.getWre1(), G.getWre1(), gradient.getWre1(), scale, true);
		update(param.getBre(), G.getBre(), gradient.getBre(), scale, false);
		update(param.getVre(), G.getVre(), gradient.getVre(), scale, true);
		update(param.getvre(), G.getvre(), gradient.getvre(), scale, false);
		
		update(param.getHun(), G.getHun(), gradient.getHun(), scale, true);
		update(param.getQun(), G.getQun(), gradient.getQun(), scale, true);
		update(param.getWun(), G.getWun(), gradient.getWun(), scale, true);
		update(param.getBun(), G.getBun(), gradient.getBun(), scale, false);
		update(param.getVun(), G.getVun(), gradient.getVun(), scale, true);
		update(param.getvun(), G.getvun(), gradient.getvun(), scale, false);
		
		update(param.getWf(), G.getWf(), gradient.getWf(), scale, true);
		update(param.getBf(), G.getBf(), gradient.getBf(), scale, false);
		
		update(param.getWi(), G.getWi(), gradient.getWi(), scale, true);
		update(param.getBi(), G.getBi(), gradient.getBi(), scale, false);
		
		update(param.getHqu(), G.getHqu(), gradient.getHqu(), scale, true);
		update(param.getWqu(), G.getWqu(), gradient.getWqu(), scale, true);
		update(param.getBqu(), G.getBqu(), gradient.getBqu(), scale, false);
		update(param.getBqe(), G.getBqe(), gradient.getBqe(), scale, false);
		
		update(param.getBa(), G.getBa(), gradient.getBa(), scale, false);
	}
}
