package trance.exceptions;

public class WordVectorSizeException extends Exception{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public WordVectorSizeException(String msg){
		super (msg);
	}
}
