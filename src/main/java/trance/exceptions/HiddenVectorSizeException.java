package trance.exceptions;

public class HiddenVectorSizeException extends Exception{
	public HiddenVectorSizeException(String msg){
		super(msg);
	}
}
