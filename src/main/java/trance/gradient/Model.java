package trance.gradient;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.HashMap;
import java.util.Map;

import org.jblas.FloatMatrix;

import trance.progs.Symbol;
import trance.progs.Terminal;
import trance.random.RandomGen;

/**
 * @author Mario Filino
 *
 */
public class Model {
	private int hiddenDimSize;
	private int wordEmbedDimSize;
	
	private Symbol grammar;

	private FloatMatrix[] Hsh;
	private FloatMatrix[] Qsh;
	private FloatMatrix[] Wsh;
	private FloatMatrix[] Bsh;
	private FloatMatrix[] Vsh;
	private FloatMatrix vsh;
	
	private FloatMatrix[] Hre;
	private FloatMatrix[] Qre;
	private FloatMatrix[] Wre0;
	private FloatMatrix[] Wre1;
	private FloatMatrix[] Bre;
	private FloatMatrix[] Vre;
	private FloatMatrix vre;
	
	private FloatMatrix[] Hun;
	private FloatMatrix[] Qun;
	private FloatMatrix[] Wun;
	private FloatMatrix[] Bun;
	private FloatMatrix[] Vun;
	private FloatMatrix vun;
	
	private FloatMatrix Wf;
	private FloatMatrix Bf;
	
	private FloatMatrix Wi;
	private FloatMatrix Bi;
	
	private FloatMatrix Hqu;
	private FloatMatrix Wqu;
	private FloatMatrix Bqu;
	private FloatMatrix Bqe; // contstant qn for computing hidden state for input queue
	
	private FloatMatrix Ba; // axiom: initial hidden state of stack
	
	public int count;
			
	public Model(int hidden, int embedding, Symbol grammar){
		hiddenDimSize = hidden;
		wordEmbedDimSize = embedding;
		
		this.grammar = grammar;
				
		// Initialize Wsh, Bsh, Vsh, and vsh
		Hsh = new FloatMatrix[this.grammar.size()];
		Qsh = new FloatMatrix[this.grammar.size()];
		Wsh = new FloatMatrix[this.grammar.size()];
		Bsh = new FloatMatrix[this.grammar.size()];
		Vsh = new FloatMatrix[this.grammar.size()];
		for(int i = 0; i < this.grammar.size(); i++){
			Hsh[i] = zeroMatrix(hiddenDimSize, hiddenDimSize);
			Qsh[i] = zeroMatrix(hiddenDimSize, hiddenDimSize);
			Wsh[i] = zeroMatrix(hiddenDimSize, wordEmbedDimSize);
			Bsh[i] = zeroMatrix(hiddenDimSize, 1);
			Vsh[i] = zeroMatrix(1, hiddenDimSize);
		}

		vsh = zeroMatrix(1 * this.grammar.size(), 1); 

		// Initialize Wre, Bre, Vre, and vre
		Hre = new FloatMatrix[this.grammar.size()];
		Qre = new FloatMatrix[this.grammar.size()];
		Wre0 = new FloatMatrix[this.grammar.size()];
		Wre1 = new FloatMatrix[this.grammar.size()];
		Bre = new FloatMatrix[this.grammar.size()];
		Vre = new FloatMatrix[this.grammar.size()];
		for(int i = 0; i < this.grammar.size(); i++){
			Hre[i] = zeroMatrix(hiddenDimSize, hiddenDimSize);
			Qre[i] = zeroMatrix(hiddenDimSize, hiddenDimSize);
			Wre0[i] = zeroMatrix(hiddenDimSize, hiddenDimSize);
			Wre1[i] = zeroMatrix(hiddenDimSize, hiddenDimSize);
			Bre[i] = zeroMatrix(hiddenDimSize, 1);
			Vre[i] = zeroMatrix(1, hiddenDimSize);
		}

		vre = zeroMatrix(1 * this.grammar.size(), 1);
		
		// Initialize Wun, Vun, and Bun
		Hun = new FloatMatrix[this.grammar.size()];
		Qun = new FloatMatrix[this.grammar.size()];
		Wun = new FloatMatrix[this.grammar.size()];
		Bun = new FloatMatrix[this.grammar.size()];
		Vun = new FloatMatrix[this.grammar.size()];
		for(int i = 0; i < this.grammar.size(); i++){
			Hun[i] = zeroMatrix(hiddenDimSize, hiddenDimSize);
			Qun[i] = zeroMatrix(hiddenDimSize, hiddenDimSize);
			Wun[i] = zeroMatrix(hiddenDimSize, hiddenDimSize);
			Bun[i] = zeroMatrix(hiddenDimSize, 1);
			Vun[i] = zeroMatrix(1, hiddenDimSize);
		}

		vun = zeroMatrix(1 * this.grammar.size(), 1);
		
		Wf = zeroMatrix(hiddenDimSize, hiddenDimSize);
		Bf = zeroMatrix(hiddenDimSize, 1);
		
		Wi = zeroMatrix(hiddenDimSize, hiddenDimSize);
		Bi = zeroMatrix(hiddenDimSize, 1);	
		
		Hqu = zeroMatrix(hiddenDimSize, hiddenDimSize);
		Wqu = zeroMatrix(hiddenDimSize, wordEmbedDimSize);
		Bqu = zeroMatrix(hiddenDimSize, 1);
		Bqe = zeroMatrix(hiddenDimSize, 1);
		
		Ba = zeroMatrix(hiddenDimSize, 1);
		
		this.count = 0;
	}
	
	public Model(Model n){
		this.hiddenDimSize = n.hiddenDimSize;
		this.wordEmbedDimSize = n.wordEmbedDimSize;
		
		this.grammar = n.grammar;

		this.Hsh = deepCopy(n.Hsh);
		this.Qsh = deepCopy(n.Qsh);
		this.Wsh = deepCopy(n.Wsh);
		this.Bsh = deepCopy(n.Bsh);
		this.Vsh = deepCopy(n.Vsh);
		this.vsh = deepCopy(n.vsh);
		
		this.Hre = deepCopy(n.Hre);
		this.Qre = deepCopy(n.Qre);
		this.Wre0 = deepCopy(n.Wre0);
		this.Wre1 = deepCopy(n.Wre1);
		this.Bre = deepCopy(n.Bre);
		this.Vre = deepCopy(n.Vre);
		this.vre = deepCopy(n.vre);
		
		this.Hun = deepCopy(n.Hun);
		this.Qun = deepCopy(n.Qun);
		this.Wun = deepCopy(n.Wun);
		this.Bun = deepCopy(n.Bun);
		this.Vun = deepCopy(n.Vun);
		this.vun = deepCopy(n.vun);
		
		this.Wf = deepCopy(n.Wf);
		this.Bf = deepCopy(n.Bf);
		
		this.Wi = deepCopy(n.Wi);
		this.Bi= deepCopy(n.Bi);
		
		this.Hqu = deepCopy(n.Hqu);
		this.Wqu = deepCopy(n.Wqu);
		this.Bqu = deepCopy(n.Bqu);
		this.Bqe = deepCopy(n.Bqe);
		
		this.Ba = deepCopy(n.Ba);
		
		this.count = n.count;
	}
	
	public void setZero(){
		// Initialize Wsh, Bsh, Vsh, and vsh
		Hsh = new FloatMatrix[this.grammar.size()];
		Qsh = new FloatMatrix[this.grammar.size()];
		Wsh = new FloatMatrix[this.grammar.size()];
		Bsh = new FloatMatrix[this.grammar.size()];
		Vsh = new FloatMatrix[this.grammar.size()];
		for(int i = 0; i < this.grammar.size(); i++){
			Hsh[i] = zeroMatrix(hiddenDimSize, hiddenDimSize);
			Qsh[i] = zeroMatrix(hiddenDimSize, hiddenDimSize);
			Wsh[i] = zeroMatrix(hiddenDimSize, wordEmbedDimSize);
			Bsh[i] = zeroMatrix(hiddenDimSize, 1);
			Vsh[i] = zeroMatrix(1, hiddenDimSize);
		}

		vsh = zeroMatrix(1 * this.grammar.size(), 1); 

		// Initialize Wre, Bre, Vre, and vre
		Hre = new FloatMatrix[this.grammar.size()];
		Qre = new FloatMatrix[this.grammar.size()];
		Wre0 = new FloatMatrix[this.grammar.size()];
		Wre1 = new FloatMatrix[this.grammar.size()];
		Bre = new FloatMatrix[this.grammar.size()];
		Vre = new FloatMatrix[this.grammar.size()];
		for(int i = 0; i < this.grammar.size(); i++){
			Hre[i] = zeroMatrix(hiddenDimSize, hiddenDimSize);
			Qre[i] = zeroMatrix(hiddenDimSize, hiddenDimSize);
			Wre0[i] = zeroMatrix(hiddenDimSize, hiddenDimSize);
			Wre1[i] = zeroMatrix(hiddenDimSize, hiddenDimSize);
			Bre[i] = zeroMatrix(hiddenDimSize, 1);
			Vre[i] = zeroMatrix(1, hiddenDimSize);
		}

		vre = zeroMatrix(1 * this.grammar.size(), 1);
		
		// Initialize Wun, Vun, and Bun
		Hun = new FloatMatrix[this.grammar.size()];
		Qun = new FloatMatrix[this.grammar.size()];
		Wun = new FloatMatrix[this.grammar.size()];
		Bun = new FloatMatrix[this.grammar.size()];
		Vun = new FloatMatrix[this.grammar.size()];
		for(int i = 0; i < this.grammar.size(); i++){
			Hun[i] = zeroMatrix(hiddenDimSize, hiddenDimSize);
			Qun[i] = zeroMatrix(hiddenDimSize, hiddenDimSize);
			Wun[i] = zeroMatrix(hiddenDimSize, hiddenDimSize);
			Bun[i] = zeroMatrix(hiddenDimSize, 1);
			Vun[i] = zeroMatrix(1, hiddenDimSize);
		}

		vun = zeroMatrix(1 * this.grammar.size(), 1);
		
		Wf = zeroMatrix(hiddenDimSize, hiddenDimSize);
		Bf = zeroMatrix(hiddenDimSize, 1);
		
		Wi = zeroMatrix(hiddenDimSize, hiddenDimSize);
		Bi = zeroMatrix(hiddenDimSize, 1);	
		
		Hqu = zeroMatrix(hiddenDimSize, hiddenDimSize);
		Wqu = zeroMatrix(hiddenDimSize, wordEmbedDimSize);
		Bqu = zeroMatrix(hiddenDimSize, 1);
		Bqe = zeroMatrix(hiddenDimSize, 1);
		
		Ba = zeroMatrix(hiddenDimSize, 1);
		
		this.count = 0;
	}
		
	private FloatMatrix deepCopy(FloatMatrix param){
		FloatMatrix temp = new FloatMatrix(param.rows, param.columns);
		
		temp.copy(param);
		
		return temp;
	}
	
	private FloatMatrix[] deepCopy(FloatMatrix[] param){
		FloatMatrix[] temp = new FloatMatrix[param.length];
		
		for(int i = 0; i < temp.length; i++){
			FloatMatrix arrTemp = new FloatMatrix(param[i].rows, param[i].columns);
			arrTemp.copy(param[i]);
			temp[i] = arrTemp;
		}
		
		return temp;
	}
	
	private FloatMatrix zeroMatrix(int rowSize, int collSize){
		FloatMatrix zeroMat = new FloatMatrix(rowSize, collSize);
		
		return zeroMat;
	}
	
	private void randMatrix(FloatMatrix param, double range){
		long seed = System.currentTimeMillis();
		RandomGen random = new RandomGen(seed);

		for(int i = 0; i < param.rows; i++){
			for(int j = 0; j < param.columns; j++){
				param.put(i, j, (float) random.uniform(-range, range));
			}
		}
	}
	
	private void randMatrix(FloatMatrix param, double range, RandomGen random){
		for(int i = 0; i < param.rows; i++){
			for(int j = 0; j < param.columns; j++){
				param.put(i, j, (float) random.uniform(-range, range));
			}
		}
	}
		
	private void writeFile(String path) throws Exception{
		File dir = new File(path);
    	dir.mkdirs();
		
		Map<String, FloatMatrix[]> arrParamM = new HashMap<String, FloatMatrix[]>();
		arrParamM.put("Hsh", Hsh);
		arrParamM.put("Qsh", Qsh);
		arrParamM.put("Wsh", Wsh);
		arrParamM.put("Bsh", Bsh);
		arrParamM.put("Vsh", Vsh);
		
		arrParamM.put("Hre", Hre);
		arrParamM.put("Qre", Qre);
		arrParamM.put("Wre0", Wre0);
		arrParamM.put("Wre1", Wre1);
		arrParamM.put("Bre", Bre);
		arrParamM.put("Vre", Vre);
		
		arrParamM.put("Hun", Hun);
		arrParamM.put("Qun", Qun);
		arrParamM.put("Wun", Wun);
		arrParamM.put("Bun", Bun);
		arrParamM.put("Vun", Vun);
								    	
    	// write arrParamM to external file
    	for(HashMap.Entry<String, FloatMatrix[]> entry : arrParamM.entrySet()){
    		File logFile = new File(dir, entry.getKey() + ".txt");
    		BufferedWriter writerParamM = new BufferedWriter(new FileWriter(logFile));
    		
    		writerParamM.write(hiddenDimSize + " " + wordEmbedDimSize + " " + grammar.size() + "\n");
    		
    		for(int index = 0; index < entry.getValue().length; index++){
    			String key = grammar.symbol().get(index);
    			FloatMatrix value = entry.getValue()[index];
    			
    			writerParamM.write(key + " ");
            	for(int i = 0; i < value.columns; i++){
            		for(int j = 0; j < value.rows; j++){
            			if(i == value.rows - 1 && j == value.columns - 1){
            				writerParamM.write(String.format("%.7f", value.get(j, i)));
            			}else{
            				writerParamM.write(String.format("%.7f", value.get(j, i)) + " ");
            			}
            			
            		}
            	}
            	writerParamM.write("\n");
    		}
    		writerParamM.close();
    	}
    	
		Map<String, FloatMatrix> arrParamD = new HashMap<String, FloatMatrix>();
		arrParamD.put("Csh", vsh);
		arrParamD.put("Cre", vre);
		arrParamD.put("Cun", vun);
    	
    	// write arrParamD to external file
    	for(HashMap.Entry<String, FloatMatrix> entry : arrParamD.entrySet()){
    		File logFile = new File(dir, entry.getKey() + ".txt");
    		BufferedWriter writerParamD = new BufferedWriter(new FileWriter(logFile));
    		
    		writerParamD.write(grammar.size() + "\n");
    		
    		for(int index = 0; index < entry.getValue().rows; index++){
    			String key = grammar.symbol().get(index);
    			float value = entry.getValue().get(index, 0);
    			
    			writerParamD.write(key + " " + String.format("%.7f", value));
    			writerParamD.write("\n");
    		}
    		writerParamD.close();    		
    	}
    	
		Map<String, FloatMatrix> arrParamS = new HashMap<String, FloatMatrix>();
		arrParamS.put("Wf", Wf);
		arrParamS.put("Bf", Bf);
		
		arrParamS.put("Wi", Wi);
		arrParamS.put("Bi", Bi);
		
		arrParamS.put("Hqu", Hqu);
		arrParamS.put("Wqu", Wqu);
		arrParamS.put("Bqu", Bqu);
		arrParamS.put("Bqe", Bqe);
		
		arrParamS.put("Ba", Ba);
    			
    	// write arrParamS to external file
    	for(HashMap.Entry<String, FloatMatrix> entry : arrParamS.entrySet()){
    		File logFile = new File(dir, entry.getKey() + ".txt");
    		BufferedWriter writerParamS = new BufferedWriter(new FileWriter(logFile));
    		
    		FloatMatrix value = entry.getValue();
    		
    		writerParamS.write(hiddenDimSize + " " + wordEmbedDimSize + "\n");
        	for(int i = 0; i < value.rows; i++){
        		for(int j = 0; j < value.columns; j++){
        			if(i == value.rows - 1 && j == value.columns - 1){
        				writerParamS.write(String.format("%.10f", value.get(i, j)));
        			}else{
        				writerParamS.write(String.format("%.10f", value.get(i, j)) + " ");
        			}
        		}
        		writerParamS.write("\n");
        	}
    		writerParamS.close();
    	}
	}
	
	private void readFile(String path) throws Exception{
		// List of parameter with form Map<String, INDArray>
		Map<String, FloatMatrix[]> arrParamM = new HashMap<String, FloatMatrix[]>();
		arrParamM.put("Hsh", Hsh);
		arrParamM.put("Qsh", Qsh);
		arrParamM.put("Wsh", Wsh);
		arrParamM.put("Bsh", Bsh);
		arrParamM.put("Vsh", Vsh);
		
		arrParamM.put("Hre", Hre);
		arrParamM.put("Qre", Qre);
		arrParamM.put("Wre0", Wre0);
		arrParamM.put("Wre1", Wre1);
		arrParamM.put("Bre", Bre);
		arrParamM.put("Vre", Vre);
		
		arrParamM.put("Hun", Hun);
		arrParamM.put("Qun", Qun);
		arrParamM.put("Wun", Wun);
		arrParamM.put("Bun", Bun);
		arrParamM.put("Vun", Vun);
		
		for(HashMap.Entry<String, FloatMatrix[]> entry : arrParamM.entrySet()){
			File modelFile = new File(path + entry.getKey() + ".txt");
	    	
	    	BufferedReader reader = new BufferedReader(new FileReader(modelFile));
	    	String line = reader.readLine();
	    	String[] initial = line.split(" ");
	    	
	    	int hidden = Integer.parseInt(initial[0]);
	    	int embedding = Integer.parseInt(initial[1]);
	    	int nonterminal = Integer.parseInt(initial[2]);
	    	
	    	if(hidden != hiddenDimSize) {
	    		reader.close();
				throw new Exception("hidden size of model doesn't match initialized hidden size");
			}
	    	
	    	if(embedding != wordEmbedDimSize) {
	    		reader.close();
				throw new Exception("word embedding size of model doesn't match initialized embedding size");
			}
	    	
	    	if(nonterminal != grammar.size()){
	    		reader.close();
	    		throw new Exception("different nonterminal size");
	    	}
	    	
	    	while((line = reader.readLine()) != null){
	    		if(line.isEmpty()){
	    			continue;
	    		}
	    		
	    		String[] split = line.split(" ");
	    		int index = grammar.index().get(split[0]);
	    		
	    		int rows = arrParamM.get(entry.getKey())[index].rows;
	    		int columns = arrParamM.get(entry.getKey())[index].columns;
	    		
	    		float[] parsed = new float[split.length - 1];
	    		for(int i = 0; i < parsed.length; i++){
	    			parsed[i] = Float.parseFloat(split[i + 1]);
	    		}
	    		
	    		arrParamM.get(entry.getKey())[index] = new FloatMatrix(rows, columns, parsed);
	    	}
	    	
	    	reader.close();
		}
		
		// List of parameter with form Map<String, Double>
		Map<String, FloatMatrix> arrParamD = new HashMap<String, FloatMatrix>();
		arrParamD.put("Csh", vsh);
		arrParamD.put("Cre", vre);
		arrParamD.put("Cun", vun);
		
		for(HashMap.Entry<String, FloatMatrix> entry : arrParamD.entrySet()){
			File modelFile = new File(path + entry.getKey() + ".txt");
	    	
	    	BufferedReader reader = new BufferedReader(new FileReader(modelFile));
	    	String line = reader.readLine();
	    	String[] initial = line.split(" ");

	    	int nonterminal = Integer.parseInt(initial[0]);

	    	if(nonterminal != grammar.size()){
	    		reader.close();
	    		throw new Exception("different nonterminal size");
	    	}
	    	
	    	while((line = reader.readLine()) != null){
	    		if(line.isEmpty()){
	    			continue;
	    		}
	    		
	    		String[] split = line.split(" ");
	    		int index = grammar.index().get(split[0]);

	    		arrParamD.get(entry.getKey()).put(index, 0, Float.parseFloat(split[1]));
	    	}
	    	
	    	reader.close();
		}
		
		// List of parameter with form INDArray
		Map<String, FloatMatrix> arrParamS = new HashMap<String, FloatMatrix>();
		arrParamS.put("Wf", Wf);
		arrParamS.put("Bf", Bf);
		
		arrParamS.put("Wi", Wi);
		arrParamS.put("Bi", Bi);
		
		arrParamS.put("Hqu", Hqu);
		arrParamS.put("Wqu", Wqu);
		arrParamS.put("Bqu", Bqu);
		arrParamS.put("Bqe", Bqe);
		
		arrParamS.put("Ba", Ba);
		
		for(HashMap.Entry<String, FloatMatrix> entry : arrParamS.entrySet()){
			File modelFile = new File(path + entry.getKey() + ".txt");
	    	
	    	BufferedReader reader = new BufferedReader(new FileReader(modelFile));
	    	String line = reader.readLine();
	    	String[] initial = line.split(" ");
	    	
	    	int hidden = Integer.parseInt(initial[0]);
	    	int embedding = Integer.parseInt(initial[1]);

	    	if(hidden != hiddenDimSize) {
	    		reader.close();
				throw new Exception("hidden size of model doesn't match initialized hidden size");
			}
	    	
	    	if(embedding != wordEmbedDimSize) {
	    		reader.close();
				throw new Exception("word embedding size of model doesn't match initialized hidden size");
			}
	    	
	    	int i = 0;
	    	while((line = reader.readLine()) != null){
	    		if(line.isEmpty()){
	    			continue;
	    		}
	    		
	    		String[] split = line.split(" ");
	    		for(int j = 0; j < split.length; j++){
	    			arrParamS.get(entry.getKey()).put(i, j, Float.parseFloat(split[j]));
	    		}
	    		
	    		i++;
	    	}
	    	
	    	reader.close();
		}
				
	}
	
	public void randomize(){
		double range_qu = Math.sqrt(6.0f / (hiddenDimSize + hiddenDimSize + wordEmbedDimSize));
		double range_sh = Math.sqrt(6.0f / (hiddenDimSize + hiddenDimSize + hiddenDimSize + wordEmbedDimSize));
		double range_re = Math.sqrt(6.0f / (hiddenDimSize + hiddenDimSize + hiddenDimSize + hiddenDimSize + hiddenDimSize));
		double range_un = Math.sqrt(6.0f / (hiddenDimSize + hiddenDimSize + hiddenDimSize + hiddenDimSize));
		double range_f = Math.sqrt(6.0f / (hiddenDimSize + hiddenDimSize));
		double range_i = Math.sqrt(6.0f / (hiddenDimSize + hiddenDimSize));
		double range_v = Math.sqrt(6.0f / (hiddenDimSize + 1));
		
		// Create random generator for Wsh, Wre, Wun, and V
		RandomGen WshRand = new RandomGen(System.currentTimeMillis());
		RandomGen WreRand = new RandomGen(System.currentTimeMillis());
		RandomGen WunRand = new RandomGen(System.currentTimeMillis());
		RandomGen Vrand = new RandomGen(System.currentTimeMillis());
		RandomGen WquRand = new RandomGen(System.currentTimeMillis());
		
		for(int i = 0; i < grammar.size(); i++){
			// randomize Hsh, Qsh, Wsh and Vsh
			randMatrix(Hsh[i], range_sh, WshRand);
			randMatrix(Qsh[i], range_sh, WshRand);
			randMatrix(Wsh[i], range_sh, WshRand);
			randMatrix(Vsh[i], range_v, Vrand);
			
			// randomize Wre and Vre
			randMatrix(Hre[i], range_re, WreRand);
			randMatrix(Qre[i], range_re, WreRand);
			randMatrix(Wre0[i], range_re, WreRand);
			randMatrix(Wre1[i], range_re, WreRand);
			randMatrix(Vre[i], range_v, Vrand);

			// randomize Wun and Vun
			randMatrix(Hun[i], range_un, WunRand);
			randMatrix(Qun[i], range_un, WunRand);
			randMatrix(Wun[i], range_un, WunRand);
			randMatrix(Vun[i], range_v, Vrand);
		}
		
		randMatrix(Wf, range_f);
		randMatrix(Wi, range_i);
		randMatrix(Hqu, range_qu, WquRand);
		randMatrix(Wqu, range_qu, WquRand);
	}
	
	public int hiddenSize(){
		return this.hiddenDimSize;
	}
	
	public int embeddingSize(){
		return this.wordEmbedDimSize;
	}
	
	public FloatMatrix getHqu(){
		return this.Hqu;
	}
	
	public FloatMatrix getWqu(){
		return this.Wqu;
	}
	
	public FloatMatrix getBqu(){
		return this.Bqu;
	}
	
	public FloatMatrix getBqe(){
		return this.Bqe;
	}
	
	public FloatMatrix getBa(){
		return this.Ba;
	}
	
	public FloatMatrix getWf(){
		return this.Wf;
	}
	
	public FloatMatrix getBf(){
		return this.Bf;
	}
	
	public FloatMatrix getWi(){
		return this.Wi;
	}
	
	public FloatMatrix getBi(){
		return this.Bi;
	}
	
	public FloatMatrix getHsh(int index){		
		return this.Hsh[index];
	}
	
	public FloatMatrix[] getHsh(){
		return this.Hsh;
	}
	
	public FloatMatrix getQsh(int index){		
		return this.Qsh[index];
	}
	
	public FloatMatrix[] getQsh(){
		return this.Qsh;
	}
	
	public FloatMatrix getWsh(int index){		
		return this.Wsh[index];
	}
	
	public FloatMatrix[] getWsh(){
		return this.Wsh;
	}
	
	public FloatMatrix getVsh(int index){
		return this.Vsh[index];
	}
	
	public FloatMatrix[] getVsh(){
		return this.Vsh;
	}
	
	public FloatMatrix getBsh(int index){
		return this.Bsh[index];
	}
	
	public FloatMatrix[] getBsh(){
		return this.Bsh;
	}
	
	public float getvsh(int index){
		return this.vsh.get(index, 0);
	}
	
	public FloatMatrix getvsh(){
		return this.vsh;
	}
	
	public FloatMatrix getHre(int index){
		return this.Hre[index];
	}
	
	public FloatMatrix[] getHre(){
		return this.Hre;
	}
	
	public FloatMatrix getQre(int index){
		return this.Qre[index];
	}
	
	public FloatMatrix[] getQre(){
		return this.Qre;
	}
	
	public FloatMatrix getWre0(int index){
		return this.Wre0[index];
	}
	
	public FloatMatrix[] getWre0(){
		return this.Wre0;
	}
	
	public FloatMatrix getWre1(int index){
		return this.Wre1[index];
	}
	
	public FloatMatrix[] getWre1(){
		return this.Wre1;
	}
	
	public FloatMatrix getVre(int index){
		return this.Vre[index];
	}
	
	public FloatMatrix[] getVre(){
		return this.Vre;
	}
	
	public FloatMatrix getBre(int index){
		return this.Bre[index];
	}
	
	public FloatMatrix[] getBre(){
		return this.Bre;
	}
	
	public float getvre(int index){
		return this.vre.get(index, 0);
	}
	
	public FloatMatrix getvre(){
		return this.vre;
	}
	
	public FloatMatrix getHun(int index){
		return this.Hun[index];
	}
	
	public FloatMatrix[] getHun(){
		return this.Hun;
	}
	
	public FloatMatrix getQun(int index){
		return this.Qun[index];
	}
	
	public FloatMatrix[] getQun(){
		return this.Qun;
	}
	
	public FloatMatrix getWun(int index){
		return this.Wun[index];
	}
	
	public FloatMatrix[] getWun(){
		return this.Wun;
	}
	
	public FloatMatrix getVun(int index){
		return this.Vun[index];
	}
	
	public FloatMatrix[] getVun(){
		return this.Vun;
	}
	
	public FloatMatrix getBun(int index){
		return this.Bun[index];
	}
	
	public FloatMatrix[] getBun(){
		return this.Bun;
	}
	
	public float getvun(int index){
		return this.vun.get(index, 0);
	}
	
	public FloatMatrix getvun(){
		return this.vun;
	}
		
	public void saveModel(String path) throws Exception{
		writeFile(path);
	}
	
	public void loadModel(String path) throws Exception{
		readFile(path);
	}
	
	public static float activation(float elem){
    	return (float) Math.min(Math.max(elem, -1), 1);
    }

    public static FloatMatrix activation(FloatMatrix elem){
        for(int i = 0; i < elem.length; i++){
        	elem.put(i, activation(elem.get(i)));
        }
        
        return elem;
    }
    
    public static float deactivation(float elem){
    	if(elem < 1.0 && elem > -1.0){
    		return 1;
    	}else{
    		return 0;
    	}
    }
    
    public static FloatMatrix deactivation(FloatMatrix elem){
    	for(int i = 0; i < elem.length; i++){
        	elem.put(i, deactivation(elem.get(i)));
        }
        
        return elem;
    }
}
